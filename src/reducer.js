/**
 * Root Reducer
 */
import { combineReducers } from "redux";

// Import Reducers
import app from "./libs/layouts/Home/reducer";
import admincourse from "./containers/admin/course/reducer";

// Combine all reducers into one root reducer
const appReducer = combineReducers({
  app,
  admincourse
});

const initialState = appReducer({}, {});

const rootReducer = (state, action) => {
  if (action.type === "LOGOUT_USER") {
    state = initialState;
  }
  return appReducer(state, action);
};

export default rootReducer;
