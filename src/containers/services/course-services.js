import utils from "../../utils/apiCaller";

export function callGetCourseApi(id, callback) {
	utils.httpRequest(`courses/${id}`, 'get', null, (response) => {
		callback(response);
	});
}