import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import Pagination from "../../../../libs/pagination/index";
import "../../../../css/adminpages.css";
import "../../../../css/custom.css";
import Loading from "../../../common/loading";
import Nodata from "../../../common/nodata";
import { getTeachersList, updateData } from "../../../actions/user-actions";
import swal from '@sweetalert/with-react';
import moment from 'moment';

const TeachersList = (props) => {
  const [page, setPage] = useState(0);
  const [items, setItems] = useState(10);
  const [count, setCount] = useState(0);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState('');
  const history = useHistory();

  const { userDetails } = props;

  useEffect(() => {
    getData();
  }, [page, items, search]);

  const getData = () => {
    setLoading(true);
    getTeachersList(page, items, search).then(res => {
      setCount(res.count || 0);
      setData([...res.data]);
      setLoading(false)
    }).catch(err => setLoading(false));
  }

  const handleCount = (e) => {
    let val = e.target.value;
    val = Number(val) <= 0 ? 1 : val;
    setItems(val);
  }

  const handlePagination = (page) => {
    setPage(page);
  }

  const handleNew = () => {
    history.push("/admin/teacher/new");
  };

  const getAge = (dob) => {
    let _s = moment(dob, 'x');
    let d = moment().diff(_s, 'years');
    return d > 0 ? d : 1;
  }

  const handleActive = (record) => {
    let body = {
      isActive: !record.isActive
    }
    updateData(record._id, body).then(res => {
      // console.log("res === ", res);
      let _data = data.map(itm => itm._id === record._id ? res : itm);
      setData([..._data]);
      swal("Success", "Update Successfully", "success");
    }).catch(err => {
      console.log("err === ", err);
    })
  }

  return (
    <div className="dash-data">
      <div className="courses-strip">
        <div className="flexible">
          <div className="course-side">
            <img src="/images/icons/teachers-icon.png" /> Teachers
          </div>
          <div className="addnew">
            <button onClick={handleNew}>
              <i className="fa fa-plus"></i> Add new teacher
            </button>
          </div>
        </div>
      </div>
      <div className="teachers">
        <span className="count-holder">
          Show <input type="number" className="count" value={items} onChange={handleCount} />
        </span>
        <div className="table-search-export">
          <input
            id="myInput"
            className="table-search"
            type="text"
            placeholder="Search.."
            onChange={(e) => setSearch(e.target.value)}
          />
          <span className="table-icon ccp">
            Export <img src="/images/icons/table-icon.png" />
          </span>
        </div>
        {loading && <Loading />}
        {data && data.length > 0 ?
          <table>
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Age</th>
                <th>Email</th>
                <th>Registered by</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="myTable">
              {console.log("ddddddddddddd", data)}
              {data.map((record, index) =>
                <tr key={record._id}>
                  <td>{page > 0 ? page * items + index + 1 : index + 1}</td>
                  <td>{record.firstName} {record.lastName}</td>
                  <td>{record.gender}</td>
                  <td>{getAge(record.dob)}</td>
                  <td>{record.email}</td>
                  <td>{record.createdBy !== userDetails._id ? "Admin" : "Self"}</td>
                  <td onClick={() => handleActive(record)}>

                    {record.isActive === true ?
                      <span className="c-user-active ccp"><img src="/images/icons/user.png" width="20" />
                      </span>
                      :
                      <img className="ccp" src="/images/icons/user-lock.png" width="30" />}
                  </td>
                </tr>
              )}
            </tbody>
          </table>
          : !loading && <Nodata />}
      </div>
      <Pagination
        count={count}
        page={page}
        items={items}
        callback={handlePagination}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return { userDetails: state.app.userData };
}

export default connect(mapStateToProps, null)(TeachersList);
