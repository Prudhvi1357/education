import React from "react";

export default (props) => {
    const {activeCount, inActiveCount, paidCount, freeCount} = props.data;

    console.log(props.data);
    return <div className="flexible dd-box">
        <div className="d-box1">
            <h4>{activeCount ? activeCount : 0}</h4>
            <p>Active courses</p>
        </div>

        <div className="d-box1">
            <h4>{inActiveCount ? inActiveCount : 0}</h4>
            <p>Pending courses</p>
        </div>

        <div className="d-box1">
            <h4>{freeCount ? freeCount : 0}</h4>
            <p>Free courses</p>
        </div>


        <div className="d-box1">
            <h4>{paidCount ? paidCount : 0}</h4>
            <p>Paid courses</p>
        </div>
    </div>

}