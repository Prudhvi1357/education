import React, { useState, Fragment, useEffect, useRef } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "../../../../../css/custom.css";
import swal from '@sweetalert/with-react';
var moment = require('moment');

const time_slots = [
    { key: "00:00", value: "12:00 PM" },
    { key: "01:00", value: "01:00 PM" },
    { key: "02:00", value: "02:00 PM" },
    { key: "03:00", value: "03:30 PM" },
    { key: "04:00", value: "04:00 PM" },
    { key: "05:00", value: "05:00 PM" },
    { key: "06:00", value: "06:00 PM" },
    { key: "07:00", value: "07:00 PM" },
    { key: "08:00", value: "08:00 PM" },
    { key: "09:00", value: "09:00 PM" },
    { key: "10:00", value: "10:00 PM" },
    { key: "11:00", value: "11:00 PM" },
    { key: "12:00", value: "12:00 AM" },
    { key: "13:00", value: "01:00 AM" },
    { key: "14:00", value: "02:00 AM" },
    { key: "15:00", value: "03:00 AM" },
    { key: "16:00", value: "04:00 AM" },
    { key: "17:00", value: "05:00 AM" },
    { key: "18:00", value: "06:00 AM" },
    { key: "19:00", value: "07:00 AM" },
    { key: "20:00", value: "08:00 AM" },
    { key: "21:00", value: "09:00 AM" },
    { key: "22:00", value: "10:00 AM" },
    { key: "23:00", value: "11:00 AM" },


]

export default (props) => {
    const [data, setData] = useState({ title: "", description: "", mode: "offline", source: "", notes: "", assignment: "", isActive: true })
    const [event, setEvent] = useState("single");
    const [livedate, setLiveDate] = useState([{ date: new Date(), st: "", ed: "" }]);
    const [resources, setResources] = useState([]);
    const [chapterImage, setImage] = useState(null);
    const chapterEl = useRef(null);
    const chapterFileEl = useRef(null);
    const modes = [
        { key: "offline", value: "Offline" },
        { key: "live", value: "Live" },
        { key: "event", value: "Event" }
    ]

    useEffect(() => {
        if (props.item) {
            const { item } = props;
            let obj = {
                title: item.title ? item.title : '',
                description: item.description ? item.description : '',
                mode: item.mode ? item.mode : '',
                source: item.source ? item.source : '',
                notes: item.notes ? item.notes : '',
                assignment: item.assignment ? item.assignment : '',
            }
            if (item.mode === "live" || item.mode === "event") {
                let { schedule } = item;
                let _schedule = schedule.map(dates => {
                    return { date: new Date(dates.startDate), st: moment(dates.startDate, 'x').format("HH:mm"), ed: moment(dates.endDate, 'x').format("HH:mm") }
                });
                setLiveDate([..._schedule]);
            }
            setEvent(item.type);
            setData(obj);
        }
        setResources([]);
        setImage(null);
    }, [props.item]);

    const handleChange = (name, value) => {
        setData(prevState => { return { ...prevState, [name]: value } });
    };

    const handleEvent = (value) => {
        if (value === 'single') {
            setLiveDate([{ date: new Date(), st: "", ed: "" }])
        }
        setEvent(value);
    }

    // const handleRadio = (name, value) => {
    //     event.stopPropagation();
    //     setData(prevState => {return {...prevState, [event.currentTarget.name]: event.currentTarget.value }});
    // };



    const handleSave = () => {
        let _data = {};
        if (data.title == "") {
            swal("Please enter Chapter name");
            return;
        }
        if (data.description == "") {
            swal("Please enter Chapter description");
            return;
        }
        for (let key in data) {
            if (data[key] !== "") {
                _data[key] = data[key];
            }
        }
        if (data.mode === "live" || data.mode === "event") {
            let _schedule = [];
            let flag = false;
            for (let i = 0; i < livedate.length; i++) {
                let date = moment(livedate[i].date).format("mm/dd/yyyy");
                let _st = moment(date + " " + livedate[i].st, "mm/dd/yyyy HH:mm").valueOf()
                let _ed = moment(date + " " + livedate[i].ed, "mm/dd/yyyy HH:mm").valueOf()
                if (_st < _ed) {
                    _schedule.push({
                        startDate: _st,
                        endDate: _ed
                    });
                } else {
                    flag = true;
                    break;
                }
            }
            if (flag) {
                swal("State Time should be less than End Time")
                return;
            }
            _data['schedule'] = _schedule;
        } else {
            _data['schedule'] = [];
        }

        if (data.mode === "event") {
            _data['type'] = event;
        }
        // console.log("_data === ", _data);
        props.saveCallback(_data, resources, chapterImage);
    }

    const handleResourseFiles = (e) => {
        if (e.target.files && e.target.files.length > 0) {
            let files = e.target.files;
            let _files = [];
            for (let i = 0; i < files.length; i++) {
                if (files[i]) {
                    if (!(/\.(exe|html)$/i.test(files[i].name))) {
                        _files.push(files[i]);
                    }
                }
            }
            setResources(prevState => [...prevState, ..._files]);
        }
    }

    const handleChapterFile = (e) => {
        if (e.target.files && e.target.files.length > 0) {
            let file = e.target.files[0];
            if (!(/\.(exe|html)$/i.test(file.name)) && (/\.(jpe?g|png|gif)$/i.test(file.name))) {
                setImage(file);
                let reader = new FileReader();
                reader.onloadend = (e) => {
                    chapterEl.current.src = reader.result;
                }
                reader.readAsDataURL(file);
            } else {
                // alert("Invalid File Format")
                swal("Warning", "Invalid File Format", "error")
            }
        }
    }

    const clearResourceFiles = (index) => {
        setResources(resources.filter((item, i) => i !== index))
    }

    const handleAddDates = () => {
        let _livedate = [...livedate];
        _livedate.push({ date: new Date(), st: "", ed: "" });
        setLiveDate([..._livedate]);
    }

    const handleRemoveDates = (index) => {
        let _livedate = [...livedate];
        _livedate.splice(index, 1);
        setLiveDate([..._livedate]);
    }

    const handleDateChange = (key, val, index) => {
        let _livedate = [...livedate];
        _livedate[index][key] = val;
        setLiveDate([..._livedate]);
    }

    const handleChaperImge = () => {
        chapterFileEl.current.click();
    }

    return (<Fragment>
        <div className="row padd-020 pt-4">
            <div className="col-lg-6">
                <p className="user-label">Chapter Name</p>
                <input type="text" className="course-search" placeholder="Type chapter name here" name="title" onChange={(e) => handleChange('title', e.target.value)} value={data.title} />
                <p className="user-label mt-22">Mode</p>
                <div className="mycheck">
                    <ul className="list">
                        {modes.map(item => <li className="list__item" key={item.key}>
                            <label className="label--radio off-rti">
                                <input type="radio" className="radio" value={item.key} name="mode" onChange={(e) => handleChange('mode', e.target.value)} checked={item.key === data.mode} />
                                {item.value}
                            </label>
                        </li>)}
                    </ul>
                </div>
                {data.mode === "offline" && <input type="text" className="course-search" placeholder="Enter vimeo link here" name="link" onChange={(e) => handleChange('source', e.target.value)} value={data.source} />}
            </div>
            <div className="col-lg-6">
                <p className="user-label">Description</p>
                <textarea className="form-control mt-10" placeholder="Type description here" name="description" onChange={(e) => handleChange('description', e.target.value)} value={data.description}></textarea>
            </div>
        </div>
        <div className="padd-020 offline-sec media-sec">
            <div className="resec" >
                <br />
                {/* {props.item && props.item.image && props.item.image !== '' ?
                    <p>{props.item.image}</p>
                    : null} */}
                <p className="user-label">Upload image</p>
                <input type="file" id="file1" onChange={handleChapterFile} ref={chapterFileEl} />
                {chapterImage ? <div className="cu-img"><img htmlFor="file1" className="btn-2" ref={chapterEl} onClick={handleChaperImge} /></div> :
                    props.item && props.item.image && props.item.image !== '' ? <div className="cu-img"><img alt="iii" htmlFor="file" className="btn-2" src={props.item.image} onClick={handleChaperImge} /></div>
                        : <label htmlFor="file1" className="btn-2"></label>}
            </div>
            {data.mode === "live" || data.mode === "event" ? <Fragment>
                {data.mode === "event" && <div className="event-sec">
                    <div className={event === "multiple" ? "taeb-switch right text-center" : "taeb-switch left text-center"}>
                        <div className={event === "single" ? "taeb single-day active" : "taeb multi-day"} taeb-direction="left" onClick={(e) => handleEvent('single')}>Single Day</div>
                        <div className={event === "multiple" ? "taeb single-day active" : "taeb multi-day"} taeb-direction="right" onClick={(e) => handleEvent('multiple')}>Multiple Day</div>
                    </div>
                    <p className="user-label">Venue</p>
                    <input type="text" className="course-search" placeholder="Please enter venue details" value={data.source} onChange={(e) => handleChange("source", e.target.value)} />
                    <br />
                    <br />
                </div>}
                <div className="resec" >

                    {livedate.map((dates, indx) => <div className="flexible" style={{ "justifyContent": "flex-start" }} key={`livedate_${indx}`}>
                        <div style={{ marginRight: "27px" }}>
                            <p className="user-label">Date</p>
                            <div style={{ position: "relative" }}>
                                <DatePicker
                                    dateFormat="MM/dd/yy"
                                    minDate={new Date()}
                                    selected={dates.date}
                                    onChange={(date) => handleDateChange('date', date, indx)}
                                    className="datepic"
                                /><i className="fa fa-calendar"></i>
                            </div>
                            {/* <p style="position: relative;">
                                <input type="text" className="datepic" id="datepicker2" placeholder="MM / DD / YY" />
                                <i className="fa fa-calendar"></i>
                            </p> */}
                        </div>
                        <div style={{ marginRight: "24px" }}>
                            <p className="user-label">Start Time</p>
                            <select className="nice-select" value={dates.st} onChange={(e) => handleDateChange("st", e.target.value, indx)}>
                                <option data-display="Select" value="" className="option selected focus">Nothing</option>
                                {time_slots.map((item, index) => <option key={"st" + index} value={item.key} className="option focus">{item.value}</option>)}
                            </select>
                        </div>
                        <div>
                            <p className="user-label">End Time</p>
                            <select className="nice-select" value={dates.ed} onChange={(e) => handleDateChange("ed", e.target.value, indx)}>
                                <option data-display="Select" value="" className="option selected focus">Nothing</option>
                                {time_slots.map((item, index) => <option key={"ed" + index} value={item.key} className="option focus">{item.value}</option>)}
                            </select>
                        </div>
                        {livedate.length > 2 && event === "multiple" && <div>
                            <span className="ccp c-del-icon" onClick={(e) => handleRemoveDates(indx)}><i className="fa fa-times" style={{ marginRight: 0, fontSize: "20px" }} /></span>
                        </div>}
                    </div>)}
                    <br />
                    {event === "multiple" &&
                        <button onClick={handleAddDates} className="c-add-btn" ><i className="fa fa-plus" /> Add</button>}
                </div>
            </Fragment> : null}

            <br />
            <p className="user-label">Notes</p>
            <textarea className="form-control mt-10" placeholder="Course notes" name="notes" onChange={(e) => handleChange('notes', e.target.value)} value={data.notes}></textarea>
            <br />
            <p className="user-label">Resources</p>
            {props.item && props.item.documents ?
                props.item.documents.map(doc => <p key={doc._id}>{doc.name}</p>)
                : null}
            <input type="file" id="file2" onChange={handleResourseFiles} multiple className="ccp" />
            <label htmlFor="file2" className="btn-2">Upload</label>
            {resources && resources.length > 0 &&
                resources.map((itm, index) => <span key={index} style={{ marginRight: "16px" }}><span style={{ marginRight: "8px" }}>{itm.name}</span><i className="fa fa-times ccp" onClick={() => clearResourceFiles(index)} id={index} ></i></span>)
            }
            <br />
            <p className="user-label" style={{ marginTop: "16px" }}>Assignment</p>
            <textarea className="form-control mt-10" placeholder="Assignment" name="assignment" onChange={(e) => handleChange('assignment', e.target.value)} value={data.assignment}></textarea>
        </div>

        <hr />
        <div className="row padd-020">
            <div className="col-lg-6 col-md-6">
                <button className="savebtn" onClick={handleSave}>Save</button>
            </div>
            <div className="col-lg-6 col-md-6 text-right">
                <button className="nxtbtn" onClick={handleSave}>Next</button>
            </div>
        </div>
    </Fragment>

    )
}