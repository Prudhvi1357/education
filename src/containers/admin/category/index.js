import React, { useState } from "react";
import Table from 'react-bootstrap/Table';
import Pagination from "../../../libs/pagination/index";
import "../../../css/adminpages.css"

export default (props) => {

    const [page, setPage] = useState(1);
    const [items, setItems] = useState(10);

    const handlePagination = (page) => {
        setPage(page)
    }

    return <div className="dash-data">
        <div className="courses-strip">
            <div className="flexible">
                <div className="course-side">
                    <img src="/images/icons/faculty-icon2.png" alt="icon" /> Categories
            </div>
                {/* <div className="addnew">
                <button><i className="fa fa-plus"></i> Add new faculty</button>
            </div> */}
            </div>
        </div>
        <div className="faculty">
            <span className="count-holder">Show <input type="number" className="count" value={items} onChange={(e) => setItems(e.target.value)}/></span>
            <input id="myInput" style={{"right": "0"}} className="table-search" type="text" placeholder="Search.." />

            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>User</th>
                        <th>Sub-user</th>
                        <th>Dummy</th>
                        <th>Board</th>
                        <th>Age level</th>
                        <th>Dummy</th>
                        <th>Category</th>
                        <th>Sub-category</th>
                        <th>Action</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="myTable">
                    <tr>
                        <td>1</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><i className="fa fa-pencil"></i> <i className="fa fa-trash"></i></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><i className="fa fa-pencil"></i> <i className="fa fa-trash"></i></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><i className="fa fa-pencil"></i> <i className="fa fa-trash"></i></td>
                    </tr>
                </tbody>
            </table>
        </div>


        <Pagination count={100} page={page} items={items} callback={handlePagination} />
        <br /><br />
    </div>
}


