import React, { useState } from "react";
import Table from 'react-bootstrap/Table';
import Pagination from "../../../libs/pagination/index";
import "../../../css/adminpages.css"
import "../../../css/custom.css";

export default (props) => {

    const [page, setPage] = useState(1);
    const [items, setItems] = useState(10);

    const handlePagination = (page) => {
        setPage(page)
    }

    return <div className="dash-data">
        <div className="courses-strip">
            <div className="flexible">
                <div className="course-side">
                    <img src="/images/icons/faculty-icon2.png" alt="icon" /> Faculty
            </div>
                <div className="addnew">
                    <button><i className="fa fa-plus"></i> Add new faculty</button>
                </div>
            </div>
        </div>
        <div className="faculty">
            <span className="count-holder">Show <input type="number" className="count" value={items} onChange={(e) => setItems(e.target.value)} /></span>
            <div className="table-search-export">
                <input id="myInput" className="table-search" type="text" placeholder="Search.." />
                <span className="table-icon">Export <img src="/images/icons/table-icon.png" alt="icon" /></span></div>
            <Table hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Designation</th>
                        <th>Qualification</th>
                        <th>University</th>
                        <th>Exp</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="myTable">
                    <tr>
                        <td>1</td>
                        <td><img src="/images/icons/teacher-icon.png" alt="Profile Pic" /> &nbsp; Avinash verma</td>
                        <td>Principal</td>
                        <td>Phd</td>
                        <td>IIT Delhi</td>
                        <td>37</td>
                        <td><i className="fa fa-eye"></i> <i className="fa fa-pencil"></i> <i className="fa fa-trash"></i> </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="/images/icons/teacher-icon.png" alt="Profile Pic" /> &nbsp; Avinash verma</td>
                        <td>Principal</td>
                        <td>Phd</td>
                        <td>IIT Delhi</td>
                        <td>37</td>
                        <td><i className="fa fa-eye"></i> <i className="fa fa-pencil"></i> <i className="fa fa-trash"></i> </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><img src="/images/icons/teacher-icon.png" alt="Profile Pic" /> &nbsp; Avinash verma</td>
                        <td>Principal</td>
                        <td>Phd</td>
                        <td>IIT Delhi</td>
                        <td>37</td>
                        <td><i className="fa fa-eye"></i> <i className="fa fa-pencil"></i> <i className="fa fa-trash"></i> </td>
                    </tr>
                </tbody>
            </Table>
        </div>


        <Pagination count={100} page={page} items={items} callback={handlePagination} />
        <br /><br />
    </div>
}


