import React, { useRef, useEffect, Fragment, useState } from "react";
import Table from 'react-bootstrap/Table';
import "../../../../css/adminpages.css";
import "../../../../css/custom.css";
import { validateIntegers, validateEmail, validateName } from "../../../../libs/validations";
import { createData, getUser, updateData, saveProfilePic } from "../../../actions/user-actions";
import Loading from "../../../common/loading";
import { useHistory } from "react-router-dom";
import swal from '@sweetalert/with-react';
const queryString = require('query-string');

export default (props) => {
  const default_state = {
    name: "John snow",
    qualifications: "Phd",
    experience: "13",
    university: "University of oxford",
    designation: "Professor",
    intrest: "Material science",
    about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
    mobile: "+91 888-444-2222",
    email: "Eduexcellence@gmail.com"
  }
  const parsed = queryString.parse(props.location.search);
  let params = props.match.params;
  const [user, setUser] = useState({ name: "", qualifications: "", experience: "", university: "", designation: "", intrest: "", about: "", mobile: "", email: '' })
  const [flag, setFlag] = useState(params.id === "new" || parsed.edit || false);
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const fileUploader = useRef(null);
  const profilePic = useRef(null);
  const nameEl = useRef(null);
  const qulfEl = useRef(null);
  const expEl = useRef(null);
  const univEl = useRef(null);
  const descEl = useRef(null);
  const intEl = useRef(null);
  const abtEl = useRef(null);
  const mobEl = useRef(null);
  const emailEl = useRef(null);
  let profileFile;

  useEffect(() => {
    console.log(props.match);
    if (params && params.id !== "new") {
      getUser(params.id).then(res => {
        // console.log(res);
        if (res.data) {
          let name = res.data.firstName ? res.data.firstName : '';
          name += res.data.lastName ? " " + res.data.lastName : ''
          setUser({
            name: name,
            qualifications: res.data.qualifications ? res.data.qualifications : '',
            experience: res.data.experience ? res.data.experience : '',
            university: res.data.institute ? res.data.institute : '',
            designation: res.data.designation ? res.data.designation : '',
            intrest: res.data.intrest ? res.data.intrest : '',
            about: res.data.about ? res.data.about : '',
            mobile: res.data.mobileNumber ? res.data.mobileNumber : '',
            email: res.data.email ? res.data.email : ''
          });
          if (res.data.profilePicUrl) profilePic.current.src = res.data.profilePicUrl;
        }
      }).catch(err => {
        console.log(err);
      });
    }
    else {
      // setUser(default_state);
      nameEl.current.focus()
    }
  }, []);

  const handleEdit = () => {
    let params = props.match.params;
    if (params && params.id === "new") {
      setUser({ name: "", qualifications: "", experience: "", university: "", designation: "", intrest: "", about: "", mobile: "", email: "" })
    }
    setFlag(true);
  }

  const handleCancel = () => {
    let params = props.match.params;
    if (params && params.id === "new") {
      // setUser(default_state)
      history.push('/admin/faculty')
    } else {
      setUser({ name: user.name, qualifications: user.qualifications, experience: user.experience, university: user.university, designation: user.designation, intrest: user.intrest, about: user.about, mobile: user.mobile, email: user.email });
      setFlag(false);
    }
  }

  const handleUpload = () => {
    fileUploader.current.click();
  }

  const handleFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      let files = e.target.files[0];
      if (files) {
        if (!(/\.(exe|html)$/i.test(files.name)) && (/\.(jpe?g|png|gif)$/i.test(files.name))) {
          // if (files.size > 104857600) {
          //     this.container.warning(`file upto 100MB allowed`, ``);
          // } else {

          let reader = new FileReader();
          reader.onloadend = (e) => {
            profilePic.current.src = reader.result;
            profileFile = files;
            // setFile(files);
          }
          reader.readAsDataURL(files);
          // }
        } else {
          swal("Error", "Invalid file format", "error");
        }
      }
    }
  }

  const handleSave = () => {
    let params = props.match.params;

    let _name = nameEl.current.textContent && nameEl.current.textContent.trim();
    let _qual = qulfEl.current.textContent;
    let _exp = expEl.current.textContent;
    let _univ = univEl.current.textContent;
    let _desc = descEl.current.textContent;
    let _inte = intEl.current.textContent;
    let _about = abtEl.current.textContent;
    let _mob = mobEl.current.textContent;
    let _email = emailEl.current.textContent;
    let obj = {};

    if (_name === "") {
      swal("Please Enter Name");
      return;
    } else if (!validateName(_name)) {
      swal("Please Enter Valid Name");
      return;
    } else {
      let _i = _name.indexOf(" ");
      let firstName = _name.substr(0, _i);
      let lastName = _name.substr(_i + 1);
      if (lastName === "") {
        swal("Please Enter Full Name");
        return;
      } else {
        obj['firstName'] = firstName;
        obj['lastName'] = lastName;
      }

    }

    if (_email === "") {
      swal("Please Enter Email");
      return;
    } else if (!validateEmail(_email)) {
      swal("Please Enter Valid Email");
      return;
    } else {
      obj['email'] = _email;
    }

    if (_qual !== "") {
      obj['qualifications'] = _qual;
    }

    if (_exp !== "") {
      if (!validateIntegers(_exp)) {
        swal("Please Enter Valid Experince in Years");
        return;
      } else {
        obj['experience'] = _exp;
      }
    }

    if (_univ !== "") {
      obj['institute'] = _univ;
    }

    if (_desc !== "") {
      obj['designation'] = _desc;
    }

    if (_inte !== "") {
      obj['intrest'] = _inte;
    }

    if (_about !== "") {
      obj['about'] = _about;
    }

    if (_mob !== "") {
      obj['mobileNumber'] = _mob;
    }
    obj['password'] = "Education@123";
    obj['role'] = 'faculty';

    // console.log("obj === ", obj);
    setLoading(true);
    if (params && params.id !== "new") {
      updateData(params.id, obj).then(res => {
        // console.log("res === ", res);
        let name = obj.firstName ? obj.firstName : '';
        name += obj.lastName ? " " + obj.lastName : ''
        setUser({
          name: name,
          qualifications: obj.qualifications ? obj.qualifications : '',
          experience: obj.experience ? obj.experience : '',
          university: obj.institute ? obj.institute : '',
          designation: obj.designation ? obj.designation : '',
          intrest: obj.intrest ? obj.intrest : '',
          about: obj.about ? obj.about : '',
          mobile: obj.mobileNumber ? obj.mobileNumber : '',
          email: obj.email ? obj.email : ''
        });
        setFlag(false);
        setLoading(false);
        savePic(params.id);
      }).catch(err => {
        console.log("err in update faculty === ", err);
        swal("Error", err.result.message, "error");
        setLoading(false);
      })
    } else {
      createData(obj).then(res => {
        // console.log(res);
        let name = obj.firstName ? obj.firstName : '';
        name += obj.lastName ? " " + obj.lastName : ''
        setUser({
          name: name,
          qualifications: obj.qualifications ? obj.qualifications : '',
          experience: obj.experience ? obj.experience : '',
          university: obj.institute ? obj.institute : '',
          designation: obj.designation ? obj.designation : '',
          intrest: obj.intrest ? obj.intrest : '',
          about: obj.about ? obj.about : '',
          mobile: obj.mobileNumber ? obj.mobileNumber : '',
          email: obj.email ? obj.email : ''
        });
        setFlag(false);
        setLoading(false);
        savePic(res._id);
        history.push('/admin/faculty/' + res._id);
      }).catch(err => {
        console.log(err)
        swal("Error", err.result.message, "error");
        setLoading(false);
      });
    }
  }

  const savePic = (userId) => {
    if (profileFile) {
      // call profile upload API
      saveProfilePic(userId, profileFile).then(res => {
        console.log("res === ", res);
        profileFile = null;
      }).catch(err => {
        console.log("err === ", err);
      })
    }
  }

  const handleList = (e) => {
    history.push('/admin/faculty');
  }

  console.log("user === ", user);

  return <div className="dash-data">
    <div className="courses-strip">
      <div className="flexible">
        <div className="course-side">
          <img src="/images/icons/faculty-icon2.png" /> Faculty detail
        </div>
        <div className="addnew">
          <button onClick={handleList}><img class="right-arrow" src="/images/icons/right-arrow.png" /> Back to Faculty List</button>
        </div>
      </div>
    </div>
    {loading && <Loading />}
    {/*<a href="javascript: void(0);"></a>*/}
    <div className="faculty-detail" id="faculty-detail">
      <div className="row">
        <div className="col-lg-3">
          <div className="faculty-img" contentEditable={false}>
            <div className="c-faculty-img">
              <img src="/images/faculty-detail.png" ref={profilePic} />
              <span className="upload-icon" onClick={handleUpload} style={flag ? {} : { "display": "none" }}><img src="/images/icons/upload-img.png" className="ccp" /></span>
            </div>
            <input type="file" id="file" ref={fileUploader} style={{ display: "none" }} onChange={handleFile} />

          </div>
        </div>
        <div className="col-lg-9">
          <div className="content-faculty">
            <h2 className={flag ? "aftclc outline" : "aftclc"} style={{ "minWidth": "50%" }} contentEditable={flag} ref={nameEl}>{user.name ? user.name : ''}</h2>
            <div className="flexible">
              <div className="cf-box">
                <h5 className="aftclc">Qualification</h5>
                <h4 className={flag ? "aftclc outline" : "aftclc"} contentEditable={flag} ref={qulfEl}>{user.qualifications ? user.qualifications : ''}</h4>
              </div>
              <div className="cf-box">
                <h5 className="aftclc">Experience</h5>
                <h4 className={flag ? "aftclc outline" : "aftclc"} contentEditable={flag} ref={expEl}>{user.experience ? user.experience : ''}</h4>
              </div>
              <div className="cf-box">
                <h5 className="aftclc">University</h5>
                <h4 className={flag ? "aftclc outline" : "aftclc"} contentEditable={flag} ref={univEl}>{user.university ? user.university : ''}</h4>
              </div>
              <div className="cf-box">
                <h5 className="aftclc">Designation</h5>
                <h4 className={flag ? "aftclc outline" : "aftclc"} contentEditable={flag} ref={descEl}>{user.designation ? user.designation : ''}</h4>
              </div>
              <div className="cf-box">
                <h5 className="aftclc">Area of intrest</h5>
                <h4 className={flag ? "aftclc outline" : "aftclc"} contentEditable={flag} ref={intEl}>{user.intrest ? user.intrest : ''}</h4>
              </div>
            </div>
          </div>
          <div className="cf-about">
            <h4 className="aftclc">About</h4>
            <p className={flag ? "aftclc outline" : "aftclc"} contentEditable={flag} ref={abtEl}>{user.about ? user.about : ''}</p>
            <br />
            {flag ?
              <Fragment>
                <h4 className="aftclc">Mobile</h4>
                <p className={flag ? "aftclc outline" : "aftclc"} contentEditable={flag} ref={mobEl}>{user.mobile ? user.mobile : ''}</p>
                <h4 className="aftclc">Email</h4>
                <p className={flag ? "aftclc outline" : "aftclc"} contentEditable={flag} ref={emailEl}>{user.email ? user.email : ''}</p>

              </Fragment>
              :
              <p className={flag ? "aftclc outline" : "aftclc"} contentEditable={flag} ref={mobEl}>{"Mob:" + user.mobile ? user.mobile : ''} | {user.email ? user.email : ''}</p>
            }
          </div>
        </div>
      </div>
      {flag ?
        <Fragment>
          <div className="save-btn" style={{ "display": "block" }}>
            <a className="ccp" onClick={(e) => handleSave(e)}>Save</a>
          </div>
          <div className="edit-btn">
            <a className="ccp" onClick={(e) => handleCancel()}>Cancel</a>
          </div>
        </Fragment>
        : <div className="edit-btn">
          <a className="ccp" onClick={(e) => handleEdit()}>Edit</a>
        </div>}
    </div>
  </div>

}


