import React, { useState, useEffect } from "react";
import Table from 'react-bootstrap/Table';
import Pagination from "../../../../libs/pagination/index";
import "../../../../css/adminpages.css"
import "../../../../css/custom.css";
import { useHistory } from 'react-router-dom';
import Loading from "../../../common/loading";
import Nodata from "../../../common/nodata";
import { getFacultyList, updateData } from "../../../actions/user-actions";
import { setTimeout } from "timers";

export default (props) => {

  const [page, setPage] = useState(0);
  const [items, setItems] = useState(10);
  const [count, setCount] = useState(0);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState('');
  const history = useHistory();

  useEffect(() => {
    getData();
  }, [page, items, search]);

  const getData = () => {
    setLoading(true);
    getFacultyList(page, items, search).then(res => {
      // console.log(res);
      setCount(res.count || 0);
      setData([...res.data]);
      setLoading(false)

    }).catch(err => setLoading(false));
  }

  const handleCount = (e) => {
    let val = e.target.value;
    val = Number(val) <= 0 ? 1 : val;
    setItems(val);
  }

  const handlePagination = (page) => {
    setPage(page);
  }

  const handleFaculty = (id, type = null) => {
    let path = "/admin/faculty/" + id;
    path += type !== null ? "?edit=true" : "";
    history.push(path);
  }

  const handleisActive = (id, status) => {
    console.log("Faculty status : ", id)
    let body = {
      isActive: status
    }
    setLoading(true);
    updateData(id, body).then(res => {
      getData();
    }).catch(err => {
      console.log("err === ", err)
      setLoading(false);
    })
  }

  return <div className="dash-data">
    <div className="courses-strip">
      <div className="flexible">
        <div className="course-side">
          <img src="/images/icons/faculty-icon2.png" alt="icon" /> Faculty
            </div>
        <div className="addnew">
          <button onClick={() => handleFaculty('new')}><i className="fa fa-plus"></i> Add new faculty</button>
        </div>
      </div>
    </div>
    <div className="faculty">
      <span className="count-holder">Show <input type="number" className="count" value={items} onChange={(e) => setItems(e.target.value)} /></span>

      <div className="table-search-export">
        <input id="myInput" className="table-search" type="text" placeholder="Search.." onChange={(e) => setSearch(e.target.value)} />
        <span className="table-icon ccp">Export <img src="/images/icons/table-icon.png" alt="icon" /></span>
      </div>
      {data && data.length > 0 ?
        <Table hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Designation</th>
              <th>Qualification</th>
              <th>University</th>
              <th>Exp</th>
              <th></th>
            </tr>
          </thead>
          <tbody id="myTable">
            {data.map((record, index) => <tr key={record._id}>
              <td>{page > 0 ? page * items + index + 1 : index + 1}</td>
              <td><img style={{ width: "28px", height: "28px", borderRadius: "50%" }} src={record.profilePicUrl ? record.profilePicUrl : "/images/icons/teacher-icon.png"} alt="Profile Pic" /> &nbsp; {record.firstName} {record.lastName}</td>
              <td>{record.designation ? record.designation : '-'}</td>
              <td>{record.qualifications ? record.qualifications : '-'}</td>
              <td>{record.institute ? record.institute : '-'}</td>
              <td>{record.experience ? record.experience : '-'}</td>
              <td style={{ minWidth: "148px" }}><i style={{ display: "inline" }} className="fa fa-eye ccp" onClick={(e) => handleFaculty(record._id)}></i> <i style={{ display: "inline" }} className="fa fa-pencil ccp" onClick={(e) => handleFaculty(record._id, 'edit')}></i>
                {/* <i style={{ display: "inline" }} className="fa fa-trash ccp" onClick={(e) => handleDelete(record._id)}></i> */}
                {record.isActive === true ?
                  <i style={{ display: "inline" }} className="fa fa-unlock-alt ccp" onClick={() => handleisActive(record._id, false)}></i>
                  :
                  <i style={{ display: "inline" }} className="fa fa-lock ccp" onClick={() => handleisActive(record._id, true)}></i>}
                {/* {record.isActive === true ?
                <span className="c-user-active"><img src="/images/icons/user-lock.png" width="20" onClick={(e) => handleDelete(record._id)} /></span>
                :
                <img src="/images/icons/user-lock.png" width="20" onClick={(e) => handleDelete(record._id)} />
              } */}
              </td>
            </tr>
            )}
          </tbody>
        </Table>
        : loading ?
          <Loading />
          : <Nodata />}
    </div>

    <Pagination count={count} page={page} items={items} callback={handlePagination} />
    <br /><br />
  </div>
}


