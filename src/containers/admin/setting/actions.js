import {callChangePassApi, callUpdateProfileApi} from "./services";
import {saveUser} from "../../../libs/layouts/Home/actions";

export function changePassword(obj) {
    return new Promise((resolve, reject) => {
        return callChangePassApi(obj, (response) => {
            return response.status ?
                resolve(response.result.data)
                :
                reject(response.result)
        })
    });
}

export function updateProfile(obj, id) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            return callUpdateProfileApi(obj, id, (response) => {
                if (response.status) {
                    dispatch(saveUser(response.result.data))
                    resolve(response)
                    return;
                } else {
                    reject(response)
                    return;
                }
            })
        });
    }
}