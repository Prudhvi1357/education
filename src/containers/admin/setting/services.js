
import utils from "../../../utils/apiCaller";

export function callChangePassApi(payload, callback) {
	utils.httpRequest(`changepassword`, 'post', payload, (response) => {
		callback(response);
	});
}

export function callUpdateProfileApi(payload, id, callback) {
	utils.httpRequest(`users/${id}`, 'put', payload, (response) => {
		callback(response);
	});
}