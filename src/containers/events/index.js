import React from "react";
import '../../css/adminpages.css';
import '../../css/landingpage.css';

export default (props) => {

  return (
    <>
      <section className="eventlist">
        <div className="event-banner">
          <div className="event-overlay">
            <div className="event-banner-cont">
              <h1>Events</h1>
            </div>
          </div>
        </div>


        <div className="event-name">
          <div className="container">
            <div className="row">
              <div className="col-lg-6">
                <img src="/images/event-img.png" width="100%" />
              </div>
              <div className="col-lg-6">
                <div className="row">
                  <div className="col-lg-2 padd-0">
                    <h3>01<br /><span>Nov</span></h3>
                  </div>
                  <div className="col-lg-8">
                    <h4>Event Name</h4>
                    <p className="event-time"><i className="fa fa-clock-o"></i> 8:00 am - 5 pm</p>
                  </div>
                </div>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
                <button>View Detail</button>
              </div>
            </div>
          </div>
        </div>

        <div className="event-name">
          <div className="container">
            <div className="row">
              <div className="col-lg-6">
                <img src="/images/event-img2.png" width="100%" />
              </div>
              <div className="col-lg-6">
                <div className="row">
                  <div className="col-lg-2 padd-0">
                    <h3>01<br /><span>Nov</span></h3>
                  </div>
                  <div className="col-lg-8">
                    <h4>Event Name</h4>
                    <p className="event-time"><i className="fa fa-clock-o"></i> 8:00 am - 5 pm</p>
                  </div>
                </div>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
                <button>View Detail</button>
              </div>
            </div>
          </div>
        </div>

        <div className="event-name">
          <div className="container">
            <div className="row">
              <div className="col-lg-6">
                <img src="/images/event-img3.png" width="100%" />
              </div>
              <div className="col-lg-6">
                <div className="row">
                  <div className="col-lg-2 padd-0">
                    <h3>01<br /><span>Nov</span></h3>
                  </div>
                  <div className="col-lg-8">
                    <h4>Event Name</h4>
                    <p className="event-time"><i className="fa fa-clock-o"></i> 8:00 am - 5 pm</p>
                  </div>
                </div>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
                <button>View Detail</button>
              </div>
            </div>
          </div>
        </div>

      </section>


    </>
  )
}