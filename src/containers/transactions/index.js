import React from "react";
import '../../css/adminpages.css';
import '../../css/landingpage.css';

export default (props) => {

  return (
    <>
      <section className="transation-history">
        <div className="container">
          <h1>Transation History</h1>
          <table className="table">
            <thead>
              <tr>
                <th>Order</th>
                <th>Date & Time</th>
                <th>Payment Method</th>
                <th>Transaction ID</th>
                <th>Paid Amount</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="spec">Social Science</td>
                <td className="p-relative">5 Feb 2020<br /> <small className="adjust">12: 00 PM</small></td>
                <td>PayPal</td>
                <td>87865656</td>
                <td className="spec"><i className="fa fa-inr"></i> 599</td>
                <td><button className="invoicebtn">invoice</button></td>
              </tr>
              <tr>
                <td className="spec">Language</td>
                <td className="p-relative">5 Feb 2020<br /> <small className="adjust">12: 00 PM</small></td>
                <td>PayPal</td>
                <td>87865656</td>
                <td className="spec"><i className="fa fa-inr"></i> 599</td>
                <td><button className="invoicebtn">invoice</button></td>
              </tr>
              <tr>
                <td className="spec">Science</td>
                <td className="p-relative">5 Feb 2020<br /> <small className="adjust">12: 00 PM</small></td>
                <td>PayPal</td>
                <td>87865656</td>
                <td className="spec"><i className="fa fa-inr"></i> 599</td>
                <td><button className="invoicebtn">invoice</button></td>
              </tr>
            </tbody>
          </table>
        </div>
      </section>

    </>
  )
}