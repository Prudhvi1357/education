import React, { useState, useEffect } from "react";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { getCourses } from "../../actions/course-actions";
import CourseItem from "./courseitem";



const courses = [
    { value: "Pre School", path: "preschool" },
    { value: "Junior", path: "junior" },
    { value: "Middle", path: "middle" },
    { value: "Senior", path: "senior" }
]

const preschools = [
    { value: "All", path: "all" },
    { value: "Social Science", path: "sscience" },
    { value: "Mathematics", path: "mathematics" },
    { value: "Art", path: "art" },
    { value: "Science", path: "science" },
    { value: "Music", path: "music" },
    { value: "Technology", path: "technology" }
]

export default (props) => {
    const [course, setCourse] = useState("preschool");
    const [preschool, setPreSchool] = useState("all");
    const [data, setData] = useState([]);

    useEffect(() => {
        getAllCourses();
    }, []);

    const getAllCourses = () => {
        getCourses(0, 20, {}).then(res => {
            setData([...res.data]);
        }).catch(err => {
            console.log("err ==== ", err);
        })
    }

    
    return <div className="container">
        <div className="row">
            <div className="col-lg-3">
                <div className="ourcourses-head">
                    <h3>Our Courses</h3>
                    {/* <ul className="pull-right">
                        {types.map(item => <li key={"types" + item.value}>
                            <a className={item.value === type ? "active" : ""} href="#">{item.value}</a>
                        </li>)}
                    </ul> */}
                </div>
            </div>
            <div className="col-lg-9">
                <div className="ourcourses-cont">
                    <ul>
                        {courses.map(item => <li key={"courses" + item.value}>
                            <a className={item.path === course ? "active" : ""} data-toggle="tab" href={"#" + item.path}>{item.value}</a>
                        </li>)}
                    </ul>

                    <div className="tab-content">
                        <div id={course} className="container tab-pane active tabbing1">

                            <ul className="innerul">
                                {preschools.map(item => <li className={item.path === preschool ? "active" : ""} key={course + item.path}>
                                    <a data-toggle="tab" href={"#" + item.path}>{item.value}</a>
                                </li>)}
                            </ul>
                            {/* Tab panes */}
                            <div className="tab-content">
                                <div id={preschool} className="container tab-pane active">
                                    <div className="carousel-wrap2">
                                        <div className="courses-slider">
                                            {data.length > 0 && <OwlCarousel
                                                className="owl-carousel"
                                                items={data.length}
                                                loop={true}
                                                margin={10}
                                                nav
                                                autoplay={true}
                                            >

                                                {data.map(item => <CourseItem item={item} />)}
                                            </OwlCarousel>}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- Tab content end --> */}
                </div>
            </div>
        </div>
    </div>

}