import React from "react";

export default (props) => {

    return <div className="row">
        <div className="col-lg-6">
            <div className="teach-stu-box stu-trri" onClick={() => props.handleType("S")}>
                <img src="images/student.png" alt="" />
                <div className={props.type === "S" ? "overlay-teach" : "overlay-stu"} id="myAnchor2">
                    <div className="selected">
                        <p>
                            <svg className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                <circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                <path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                            </svg>
                        </p>
                        <h3>I am Student</h3>
                    </div>
                </div>
            </div>
        </div>
        <div className="col-lg-6">
            <div className="teach-stu-box teach-trri" onClick={() => props.handleType("T")}>
                <img src="images/teacher.png" alt="" />
                <div className={props.type === "T" ? "overlay-teach" : "overlay-stu"} id="myAnchor">
                    <div className="selected">
                        <p>
                            <svg className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                <circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                <path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                            </svg>
                        </p>
                        <h3>I am Teacher</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

}