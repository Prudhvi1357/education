import React, { useState } from "react";
import Calendar from 'react-calendar';

export default (props) => {
    const [date, setDate] = useState(new Date());

    return <div className="row event-cont">
        <div className="col-lg-7">

            <div className="event-card">
                <div className="row">
                    <div className="col-lg-2">
                        <p className="dt">
                            <b>02</b> Dec
                            <br />
                            <small>2019</small>
                        </p>
                    </div>
                    <div className="col-lg-2">
                        <img src="images/icons/event-icon.png" alt="" />
                    </div>
                    <div className="col-lg-8">
                        <h4>Importance of Foundation Course</h4>
                        <p>
                            <i className="fa fa-clock-o"></i> Wednesday, 3:00 pm to 6:00 pm</p>
                        <p>
                            <i className="fa fa-map-marker"></i> PSC Convention Hall</p>
                    </div>
                </div>
            </div>

            <div className="event-card active-event">
                <div className="row">
                    <div className="col-lg-2">
                        <p className="dt">
                            <b>02</b> Dec
                            <br />
                            <small>2019</small>
                        </p>
                    </div>
                    <div className="col-lg-2">
                        <img src="images/icons/event-icon.png" alt="" />
                    </div>
                    <div className="col-lg-8">
                        <h4>Importance of Foundation Course</h4>
                        <p>
                            <i className="fa fa-clock-o"></i> Wednesday, 3:00 pm to 6:00 pm</p>
                        <p>
                            <i className="fa fa-map-marker"></i> PSC Convention Hall</p>
                    </div>
                </div>
            </div>

            <div className="event-card">
                <div className="row">
                    <div className="col-lg-2">
                        <p className="dt">
                            <b>02</b> Dec
                            <br />
                            <small>2019</small>
                        </p>
                    </div>
                    <div className="col-lg-2">
                        <img src="images/icons/event-icon.png" alt="" />
                    </div>
                    <div className="col-lg-8">
                        <h4>Importance of Foundation Course</h4>
                        <p>
                            <i className="fa fa-clock-o"></i> Wednesday, 3:00 pm to 6:00 pm</p>
                        <p>
                            <i className="fa fa-map-marker"></i> PSC Convention Hall</p>
                    </div>
                </div>
            </div>

        </div>
        <div className="col-lg-5">
            <Calendar
                onChange={(date) => setDate(date)}
                value={date}
                className="datepicker"
            />
            <div className="footer-evt">
                <p>
                    <button className="allevents">All Events</button>
                </p>
            </div>

        </div>
    </div>

}