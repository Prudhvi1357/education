import React from "react";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';


export default (props) => {

    return <OwlCarousel
        className="owl-carousel"
        items={1}
        loop={true}
        nav
        autoplay={true}
    >
        <div className="item"><img src="/images/banner.png" alt="" /></div>
        <div className="item"><img src="/images/banner.png" alt="" /></div>
        <div className="item"><img src="/images/banner.png" alt="" /></div>
    </OwlCarousel>

}