import React from "react";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';


export default (props) => {

    return <OwlCarousel
        className="owl-carousel"
        items={3}
        slideBy={1}
        loop
        nav
        autoplay
        center
    >
        <div className="box">
            <div className="level-item">
                <div className="heading card-head">
                    <img className="avatar" src="images/icons/r1.png" alt="" />
                    <h4>Rajindar Mehwar</h4>
                    <p>Principal</p>
                    <h5>Course taken :<span>Economics</span>
                    </h5>
                </div>

                <div className="content">
                    <p>My heartiest thanks to team eduExcellence for organizing three days learning workshop on Human Resource
                        Management by Dr.Harish sir. We have learned and received a lot of positivity from you.
                        <a href="#">Read More</a>
                    </p>
                </div>
            </div>
        </div>

        <div className="box">
            <div className="level-item">
                <div className="heading card-head">
                    <img className="avatar" src="images/icons/r2.png" alt="" />
                    <h4>Rajindar Mehwar</h4>
                    <p>Principal</p>
                    <h5>Course taken :<span>Economics</span>
                    </h5>
                </div>

                <div className="content">
                    <p>My heartiest thanks to team eduExcellence for organizing three days learning workshop on Human Resource
                        Management by Dr.Harish sir. We have learned and received a lot of positivity from you.
                        <a href="#">Read More</a>
                    </p>
                </div>
            </div>
        </div>

        <div className="box">
            <div className="level-item">
                <div className="heading card-head">
                    <img className="avatar" src="images/icons/r3.png" alt="" />
                    <h4>Rajindar Mehwar</h4>
                    <p>Principal</p>
                    <h5>Course taken :<span>Economics</span>
                    </h5>
                </div>

                <div className="content">
                    <p>My heartiest thanks to team eduExcellence for organizing three days learning workshop on Human Resource
                        Management by Dr.Harish sir. We have learned and received a lot of positivity from you.
                        <a href="#">Read More</a>
                    </p>
                </div>
            </div>
        </div>
    </OwlCarousel>

}