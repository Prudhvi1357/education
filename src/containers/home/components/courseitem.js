import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

export default (props) => {
    const history = useHistory();
    const {item} = props;
    const handleView = () => {
        history.push(`/courses/${item._id}`);
    }
    const getPrice = () => {
        const {pricing} = item;
        let d = new Date().valueOf()
        if (pricing.free) {
            return pricing.freeUntil && pricing.freeUntil < d ? pricing.price : "Free";
        } else if (pricing.discount) {
            return pricing.discountUntil && pricing.discountUntil < d ? pricing.price : pricing.discountedPrice;
        } else {
            return pricing.price;
        }
    }
    
    return <div className="item" key={item._id}>
        <div className="course-card">
            <div className="card-img">
                <img src={item.thumbnail ? item.thumbnail : "images/course1.png"} />
                <div className="card-overlay">
                    <div className="strip">Senior Section</div>
                    <p>
                        <button className="detailbtn" onClick={handleView}>View Detail</button>
                    </p>
                </div>
            </div>
            <div className="course-card-body">
                <button className="basicbtn">Social</button>
                <h4>{item.title}</h4>
                <p>{item.description} <a href="#">Read More</a></p>
                <hr />
                <div className="row">
                    <div className="col-lg-6">
                        <p>
                            <i className="fa fa-user"></i> 22 &nbsp; &nbsp;
                    <i className="fa fa-star"></i> 5
                </p>
                        <small>{item.targetUser && item.targetUser !== "" ? `For ${item.targetUser}` : ''}</small>
                    </div>
                    {item.pricing && item.pricing.price > 0 &&
                        <div className="col-lg-6">
                            <button className="inr">
                                <i className="fa fa-inr"></i> {getPrice()}</button>
                        </div>}
                </div>
            </div>
        </div>
    </div>
}