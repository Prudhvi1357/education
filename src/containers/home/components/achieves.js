import React from "react";

export default (props) => {

    return <div className="achieve-inner">
        <div className="row">
            <div className="col-lg-4">
                <div className="topic1">
                    <img src="images/icons/topic1.png" alt="" />
                    <h3>1300+ Topics</h3>
                    <p>Wide Choice of Subject</p>
                </div>
            </div>
            <div className="col-lg-4">
                <div className="topic1">
                    <img src="images/icons/topic2.png" alt="" />
                    <h3>1275 Tests Taken</h3>
                    <p>That's a Lot</p>
                </div>
            </div>
            <div className="col-lg-4">
                <div className="topic1 no-bor">
                    <img src="images/icons/topic3.png" alt="" />
                    <h3>256+ Instructors</h3>
                    <p>All Trained Professionals</p>
                </div>
            </div>
        </div>
    </div>

}