import React from "react";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';


export default (props) => {

    return <OwlCarousel
        className="owl-carousel"
        items={4}
        loop={true}
        margin={20}
        nav
        autoplay={true}
    >
        <div className="item">
            <div className="blog-card">
                <img src="images/blog1.png" alt="" />
                <h4>Acharya Narendra Dev College</h4>
            </div>
        </div>
        <div className="item">
            <div className="blog-card">
                <img src="images/blog2.png" alt="" />
                <h4>Delhi College of Arts & Commerce</h4>
            </div>
        </div>
        <div className="item">
            <div className="blog-card">
                <img src="images/blog3.png" alt="" />
                <h4>Atma Ram Sanatan Dharma College</h4>
            </div>
        </div>
        <div className="item">
            <div className="blog-card">
                <img src="images/blog4.png" alt="" />
                <h4>Aditi Mahavidyalaya</h4>
            </div>
        </div>
        <div className="item">
            <div className="blog-card">
                <img src="images/blog1.png" alt="" />
                <h4>Acharya Narendra Dev College</h4>
            </div>
        </div>
    </OwlCarousel>

}