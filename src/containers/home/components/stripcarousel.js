import React from "react";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';


export default (props) => {

    return <OwlCarousel
        className="owl-carousel"
        items={5}
        loop={true}
        margin={20}
        nav
        autoplay={true}
    >
        <div className="item livepol">
            <h4>2010</h4>
            <h5>UK</h5>
            <p>Liverpool</p>
        </div>
        <div className="item livepol">
            <h4>2011</h4>
            <h5>Australia</h5>
            <p>Melbourne, Sydney</p>
        </div>
        <div className="item livepol">
            <h4>2011</h4>
            <h5>UK</h5>
            <p>Hull</p>
        </div>
        <div className="item livepol">
            <h4>2012</h4>
            <h5>Australia</h5>
            <p>Sydney</p>
        </div>
        <div className="item livepol">
            <h4>2012</h4>
            <h5>USA</h5>
            <p>Oswego</p>
        </div>
        <div className="item livepol">
            <h4>2013</h4>
            <h5>Australia</h5>
            <p>Gold Coast, Brisbane</p>
        </div>
        <div className="item livepol">
            <h4>2014</h4>
            <h5>USA</h5>
            <p>Oswego</p>
        </div>
        <div className="item livepol">
            <h4>2015</h4>
            <h5>USA</h5>
            <p>Manhattan, Old westbury</p>
        </div>
        <div className="item livepol">
            <h4>2016</h4>
            <h5>Finland</h5>
            <p>Helsinki, Jyvaskyla</p>
        </div>
        <div className="item livepol">
            <h4>2016</h4>
            <h5>USA</h5>
            <p>Old westbury, Washington D.C.</p>
        </div>
    </OwlCarousel>

}