import { callVerifyEmailApi } from './services';


export function VerifyEmailAction(token) {
    return new Promise((resolve, reject)=> {
        callVerifyEmailApi(token, (response) => {
            response.status ? resolve(response) : reject(response)
        });
    });
}