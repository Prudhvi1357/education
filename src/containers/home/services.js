import utils from '../../utils/apiCaller';

export function callVerifyEmailApi(token, callback) {
	utils.httpRequest(`verifyemail/${token}`, 'get', null, (response) => {
        callback(response);
	});
}
