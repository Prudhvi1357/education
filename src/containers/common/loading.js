import React from "react";
import '../../css/custom.css';

export default (props) => {

    return <div className="loading-block">
        <div className="spinner-border" style={{ color: "#f79d2b" }}></div>
    </div>
}


