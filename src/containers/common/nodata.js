import React from "react";
import '../../css/custom.css';

export default (props) => {

    return <div className="no-data-block">
        <div>
            <div><i className="fa fa-frown-o" aria-hidden="true"></i></div>
            <div>No Data Found</div>
        </div>
    </div>
}


