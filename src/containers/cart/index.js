import React from "react";
import '../../css/adminpages.css';
import '../../css/landingpage.css';

export default (props) => {

  return (
    <>
      <section className="cart-items">
        <div className="container">
          <h1>Courses in cart</h1>
          <div className="row">
            <div className="col-lg-9">
              <div className="cart-detail">
                <div className="row">
                  <div className="col-lg-7">
                    <div className="cart-box">
                      <img src="images/cart-img.png" />
                      <h4>Political Science</h4>
                      <p>By anderi neagoies and 3 other</p>
                    </div>
                  </div>
                  <div className="col-lg-5">
                    <div className="cart-price">
                      <h5><i className="fa fa-inr"></i> 599</h5>
                      <p>12,480 &nbsp; 96% Off</p>
                      <p style={{ fontWeight: 500 }}><a href="#">Remove</a> | <a href="#">Move to My Favourite</a></p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="cart-detail">
                <div className="row">
                  <div className="col-lg-7">
                    <div className="cart-box">
                      <img src="images/cart-img.png" />
                      <h4>Political Science</h4>
                      <p>By anderi neagoies and 3 other</p>
                    </div>
                  </div>
                  <div className="col-lg-5">
                    <div className="cart-price">
                      <h5><i className="fa fa-inr"></i> 599</h5>
                      <p>12,480 &nbsp; 96% Off</p>
                      <p style={{ fontWeight: 500 }}><a href="#">Remove</a> | <a href="#">Move to My Favourite</a></p>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div className="col-lg-3 bl">
              <div className="cart-total">
                <h4>Totals</h4>
                <h3><i className="fa fa-inr"></i> 1198</h3>
                <p><span style={{ textDecoration: "line-through" }}>12,960</span> <br />
                  96% Off</p>
                <p><button>Checkout</button></p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="cart-carousel categories">
        <div class="container">
          <hr />
          <div className="course-list">
            <h3 className="related-course">Related Courses</h3>
            <div className="row">
              <div className="col-lg-3 toadd">
                <div className="course-card spe">
                  <div className="card-img">
                    <img src="images/course1.png" width="100%" />
                    <div className="card-overlay">
                      <div className="strip">
                        Senior Section
                                 </div>
                      <p><button className="detailbtn">View Detail</button></p>
                    </div>
                  </div>
                  <div className="course-card-body">
                    <button className="basicbtn">Social</button>
                    <div className="temp12">
                      <h4>Political Science</h4>
                      <p>The lysine contingency it’s intended to prevent the <a href="#" className="rm">Read More</a></p>
                      <p className="description2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                      <div className="button-group">
                        <button className="bkt-btn">Marketing</button> <button className="bkt-btn">Lyfestyle</button> <button className="bkt-btn">Marketing</button>
                      </div>
                    </div>
                    <hr />
                    <div className="temp22">
                      <h3><i className="fa fa-inr"></i> 599 <small style={{ textDecoration: "line-through", color: "#999" }}><i className="fa fa-inr"></i> 12,480</small><span className="wishing"><i className="fa fa-heart"></i></span></h3>
                      <p style={{ color: "#a90c17", fontWeight: 500 }}><i className="fa fa-clock-o"></i> 6 days left at this price!</p>
                    </div>
                    <div className="row hoverd">
                      <div className="col-lg-6">
                        <button className="buy-add-btn">By now</button>
                      </div>
                      <div className="col-lg-6">
                        <button className="buy-add-btn">Add to cart</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 toadd2">
                <div className="course-card spe2">
                  <div className="card-img">
                    <img src="images/course2.png" width="100%" />
                    <div className="card-overlay">
                      <div className="strip">
                        Senior Section
                                 </div>
                      <p><button className="detailbtn">View Detail</button></p>
                    </div>
                  </div>
                  <div className="course-card-body">
                    <button className="basicbtn">Social</button>
                    <div className="temp12">
                      <h4>Political Science</h4>
                      <p>The lysine contingency it’s intended to prevent the <a href="#" className="rm">Read More</a></p>
                      <p className="description2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                      <div className="button-group">
                        <button className="bkt-btn">Marketing</button> <button className="bkt-btn">Lyfestyle</button> <button className="bkt-btn">Marketing</button>
                      </div>
                    </div>
                    <hr />
                    <div className="temp22">
                      <h3><i className="fa fa-inr"></i> 599 <small style={{ textDecoration: "line-through", color: "#999" }}><i className="fa fa-inr"></i> 12,480</small><span className="wishing"><i className="fa fa-heart"></i></span></h3>
                      <p style={{ color: "#a90c17", fontWeight: 500 }}><i className="fa fa-clock-o"></i> 6 days left at this price!</p>
                    </div>
                    <div className="row hoverd">
                      <div className="col-lg-6">
                        <button className="buy-add-btn">By now</button>
                      </div>
                      <div className="col-lg-6">
                        <button className="buy-add-btn">Add to cart</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 toadd3">
                <div className="course-card spe3">
                  <div className="card-img">
                    <img src="images/course3.png" width="100%" />
                    <div className="card-overlay">
                      <div className="strip">
                        Senior Section
                                 </div>
                      <p><button className="detailbtn">View Detail</button></p>
                    </div>
                  </div>
                  <div className="course-card-body">
                    <button className="basicbtn">Social</button>
                    <div className="temp12">
                      <h4>Political Science</h4>
                      <p>The lysine contingency it’s intended to prevent the <a href="#" className="rm">Read More</a></p>
                      <p className="description2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                      <div className="button-group">
                        <button className="bkt-btn">Marketing</button> <button className="bkt-btn">Lyfestyle</button> <button className="bkt-btn">Marketing</button>
                      </div>
                    </div>
                    <hr />
                    <div className="temp22">
                      <h3><i className="fa fa-inr"></i> 599 <small style={{ textDecoration: "line-through", color: "#999" }}><i className="fa fa-inr"></i> 12,480</small><span className="wishing"><i className="fa fa-heart"></i></span></h3>
                      <p style={{ color: "#a90c17", fontWeight: 500 }}><i className="fa fa-clock-o"></i> 6 days left at this price!</p>
                    </div>
                    <div className="row hoverd">
                      <div className="col-lg-6">
                        <button className="buy-add-btn">By now</button>
                      </div>
                      <div className="col-lg-6">
                        <button className="buy-add-btn">Add to cart</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 toadd4">
                <div className="course-card spe4">
                  <div className="card-img">
                    <img src="images/course3.png" width="100%" />
                    <div className="card-overlay">
                      <div className="strip">
                        Senior Section
                                 </div>
                      <p><button className="detailbtn">View Detail</button></p>
                    </div>
                  </div>
                  <div className="course-card-body">
                    <button className="basicbtn">Social</button>
                    <div className="temp12">
                      <h4>Political Science</h4>
                      <p>The lysine contingency it’s intended to prevent the <a href="#" className="rm">Read More</a></p>
                      <p className="description2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                      <div className="button-group">
                        <button className="bkt-btn">Marketing</button> <button className="bkt-btn">Lyfestyle</button> <button className="bkt-btn">Marketing</button>
                      </div>
                    </div>
                    <hr />
                    <div className="temp22">
                      <h3><i className="fa fa-inr"></i> 599 <small style={{ textDecoration: "line-through", color: "#999" }}><i className="fa fa-inr"></i> 12,480</small><span className="wishing"><i className="fa fa-heart"></i></span></h3>
                      <p style={{ color: "#a90c17", fontWeight: 500 }}><i className="fa fa-clock-o"></i> 6 days left at this price!</p>
                    </div>
                    <div className="row hoverd">
                      <div className="col-lg-6">
                        <button className="buy-add-btn">By now</button>
                      </div>
                      <div className="col-lg-6">
                        <button className="buy-add-btn">Add to cart</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section >
    </>
  )
}