import {callupdateApi, callCreateApi, callListApi} from "../common/services";
import {callGetCourseApi} from "../services/course-services";
import {saveCourse} from "../admin/course/actions";

export function updateCourse(id, payload) {
    return new Promise((resolve, reject) => {
        return callupdateApi(`courses/${id}`, payload, (response) => {
            if (response.status) {
                resolve(response.result.data)
            } else {
                reject(response)
            }
        })
    });
}

export function getCourses(offset,limit,filter) {
    return new Promise((resolve, reject) => {
        return callCreateApi(`courses-dashboard?offset=${offset}&limit=${limit}`, filter,(response) => {
            if (response.status) {
                resolve(response.result)
            } else {
                reject(response)
            }
        })
    });
}

export function getCourse(id) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            return callGetCourseApi(id, (response) => {
                if (response.status) {
                    dispatch(saveCourse(response.result.data))
                    resolve(response)
                    return;
                } else {
                    reject(response)
                    return;
                }
            })
        });
    }
}

export function getList(path) {
    return new Promise((resolve, reject) => {
        return callListApi(path, 0, 100, (response) => {
            if (response.status) {
                resolve(response.result.data.docs)
            } else {
                reject(response)
            }
        })
    });
}