import React from "react";

export default (props) => {

    return <div className="reviews">
        <h3>Reviews</h3>
        <div className="row">
            <div className="col-lg-4">
                <div className="user-star">
                    <h5>4.4</h5>
                    <p><i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-half"></i></p>
                    <p>Course Rating</p>
                </div>
            </div>
            <div className="col-lg-4">
                <div className="user-rating">
                    <img src="images/icons/rt.png" alt="" />
                    <p>a week ago</p>
                    <p><span>Rahim Talley</span></p>
                </div>
                <div className="user-rating">
                    <img src="images/icons/rt.png" alt="" />
                    <p>a week ago</p>
                    <p><span>Rahim Talley</span></p>
                </div>
            </div>
            <div className="col-lg-4">
                <div className="comment-sec">
                    <p><i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-half"></i></p>
                    <p>So far</p>
                </div>
                <div className="comment-sec mt-24">
                    <p><i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star"></i> <i className="fa fa-star-half"></i></p>
                </div>
            </div>
        </div>
    </div>

}