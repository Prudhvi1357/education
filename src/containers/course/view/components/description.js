import React from "react";

const Description = (props) => {

    return <div className="tabdb description active">
        <p className="description_p">{props && props.course && props.course.description ? props.course.description : ""}</p>
    </div>
}

export default Description;