import React, { useEffect, useState, Fragment } from "react";
import Description from "./description";
import Curriculum from "./curriculum";
import Faculty from "./faculty";
import Dates from "./dates";

const CourseViewDetail = (props) => {

    const [tab, setTab] = useState('D');


    const setContent = () => {
        switch (tab) {
            case "B": return <div className="no-pad">
                <p className="description_p quest">{props && props.course && props.course.benefits ? props.course.benefits : ""}</p>
            </div>
                break;
            case 'C': return <Curriculum course={props.course} />;
                break;
            case 'F': return <Faculty course={props.course} />;
                break;
            case 'I': return <Dates course={props.course} />;
                break;
            default: return <Description course={props.course} />

        }
    }

    return <section className="detail-sec2 categories">
        <div className="tab-menu">
            <ul className="tab-nav">
                {/* <span className="bottom-border"></span> */}
                <li className={tab === "D" ? "tab-nav__item active" : "tab-nav__item"}><a onClick={() => setTab('D')}>Description</a></li>
                <li className={tab === "B" ? "tab-nav__item active" : "tab-nav__item"}><a onClick={() => setTab('B')}>Benefits</a></li>
                <li className={tab === "C" ? "tab-nav__item active" : "tab-nav__item"}><a onClick={() => setTab('C')}>Curriculum</a></li>
                <li className={tab === "F" ? "tab-nav__item active" : "tab-nav__item"}><a onClick={() => setTab('F')}>Faculty</a></li>
                <li className={tab === "I" ? "tab-nav__item active" : "tab-nav__item"}><a onClick={() => setTab('I')}>Important Dates</a></li>
            </ul>
        </div>
        <div className="container">
            <div className="form-sec">
                {setContent()}
                <br /><br />
                <hr />
                <div className="course-list">
                    <h3 className="related-course">Related Courses</h3>
                    <div className="row">
                        <div className="col-lg-3 toadd">
                            <div className="course-card spe">
                                <div className="card-img">
                                    <img src="/images/course1.png" width="100%" />
                                    <div className="card-overlay">
                                        <div className="strip">Senior Section</div>
                                        <p><button className="detailbtn">View Detail</button></p>
                                    </div>
                                </div>
                                <div className="course-card-body">
                                    <button className="basicbtn">Social</button>
                                    <div className="temp12">
                                        <h4>Political Science</h4>
                                        <p>The lysine contingency it’s intended to prevent the <a href="#" className="rm">Read More</a></p>
                                        <p className="description2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        <div className="button-group">
                                            <button className="bkt-btn">Marketing</button> <button className="bkt-btn">Lyfestyle</button> <button className="bkt-btn">Marketing</button>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="temp22">
                                        <h3><i className="fa fa-inr"></i> 599 <small style={{ textDecoration: "line-through", color: "#999" }}><i className="fa fa-inr"></i> 12,480</small><span className="wishing"><i className="fa fa-heart"></i></span></h3>
                                        <p style={{ color: "#a90c17", fontWeight: 500 }}><i className="fa fa-clock-o"></i> 6 days left at this price!</p>
                                    </div>
                                    <div className="row hoverd">
                                        <div className="col-lg-6">
                                            <button className="buy-add-btn">By now</button>
                                        </div>
                                        <div className="col-lg-6">
                                            <button className="buy-add-btn">Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 toadd2">
                            <div className="course-card spe2">
                                <div className="card-img">
                                    <img src="/images/course2.png" width="100%" />
                                    <div className="card-overlay">
                                        <div className="strip">Senior Section</div>
                                        <p><button className="detailbtn">View Detail</button></p>
                                    </div>
                                </div>
                                <div className="course-card-body">
                                    <button className="basicbtn">Social</button>
                                    <div className="temp12">
                                        <h4>Political Science</h4>
                                        <p>The lysine contingency it’s intended to prevent the <a href="#" className="rm">Read More</a></p>
                                        <p className="description2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        <div className="button-group">
                                            <button className="bkt-btn">Marketing</button> <button className="bkt-btn">Lyfestyle</button> <button className="bkt-btn">Marketing</button>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="temp22">
                                        <h3><i className="fa fa-inr"></i> 599 <small style={{ textDecoration: "line-through", color: "#999" }}><i className="fa fa-inr"></i> 12,480</small><span className="wishing"><i className="fa fa-heart"></i></span></h3>
                                        <p style={{ color: "#a90c17", fontWeight: 500 }}><i className="fa fa-clock-o"></i> 6 days left at this price!</p>
                                    </div>
                                    <div className="row hoverd">
                                        <div className="col-lg-6">
                                            <button className="buy-add-btn">By now</button>
                                        </div>
                                        <div className="col-lg-6">
                                            <button className="buy-add-btn">Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 toadd3">
                            <div className="course-card spe3">
                                <div className="card-img">
                                    <img src="/images/course3.png" width="100%" />
                                    <div className="card-overlay">
                                        <div className="strip">Senior Section</div>
                                        <p><button className="detailbtn">View Detail</button></p>
                                    </div>
                                </div>
                                <div className="course-card-body">
                                    <button className="basicbtn">Social</button>
                                    <div className="temp12">
                                        <h4>Political Science</h4>
                                        <p>The lysine contingency it’s intended to prevent the <a href="#" className="rm">Read More</a></p>
                                        <p className="description2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        <div className="button-group">
                                            <button className="bkt-btn">Marketing</button> <button className="bkt-btn">Lyfestyle</button> <button className="bkt-btn">Marketing</button>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="temp22">
                                        <h3><i className="fa fa-inr"></i> 599 <small style={{ textDecoration: "line-through", color: "#999" }}><i className="fa fa-inr"></i> 12,480</small><span className="wishing"><i className="fa fa-heart"></i></span></h3>
                                        <p style={{ color: "#a90c17", fontWeight: 500 }}><i className="fa fa-clock-o"></i> 6 days left at this price!</p>
                                    </div>
                                    <div className="row hoverd">
                                        <div className="col-lg-6">
                                            <button className="buy-add-btn">By now</button>
                                        </div>
                                        <div className="col-lg-6">
                                            <button className="buy-add-btn">Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 toadd4">
                            <div className="course-card spe4">
                                <div className="card-img">
                                    <img src="/images/course3.png" width="100%" />
                                    <div className="card-overlay">
                                        <div className="strip">Senior Section</div>
                                        <p><button className="detailbtn">View Detail</button></p>
                                    </div>
                                </div>
                                <div className="course-card-body">
                                    <button className="basicbtn">Social</button>
                                    <div className="temp12">
                                        <h4>Political Science</h4>
                                        <p>The lysine contingency it’s intended to prevent the <a href="#" className="rm">Read More</a></p>
                                        <p className="description2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        <div className="button-group">
                                            <button className="bkt-btn">Marketing</button> <button className="bkt-btn">Lyfestyle</button> <button className="bkt-btn">Marketing</button>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="temp22">
                                        <h3><i className="fa fa-inr"></i> 599 <small style={{ textDecoration: "line-through", color: "#999" }}><i className="fa fa-inr"></i> 12,480</small><span className="wishing"><i className="fa fa-heart"></i></span></h3>
                                        <p style={{ color: "#a90c17", fontWeight: 500 }}><i className="fa fa-clock-o"></i> 6 days left at this price!</p>
                                    </div>
                                    <div className="row hoverd">
                                        <div className="col-lg-6">
                                            <button className="buy-add-btn">By now</button>
                                        </div>
                                        <div className="col-lg-6">
                                            <button className="buy-add-btn">Add to cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

}

export default CourseViewDetail;