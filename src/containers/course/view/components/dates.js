import React, {useState, useEffect} from "react";
import {getList} from "../../../actions/course-actions";
import moment from "moment";

const Dates = (props) => {

    const [loading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    useEffect(() => {
        if (props.course) {
            getData(props.course._id);
        } else {
            setLoading(false);
        }
    }, [props.course])

    const getData = (id) => {
        getList(`impdates?courseid=${id}`).then(res => {
            setData(res);
            setLoading(false)
        }).catch(err => {
            setLoading(false)
        })
    }

    const getDays = (start, end) => {
        let _s = moment(start, 'x');
        let _e = moment(end, 'x');
        let d = _e.diff(_s, 'days');
        return d > 0 ? d + " Days" : 1 + " Day";
    }

    console.log("data ==== ", data);

    return <div className="imp-dates">
        {data && data.length > 0 ? <table className="table">
            <thead>
                <tr>
                    <th>Phase</th>
                    <th>Dates</th>
                    <th>Days</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {data.map(record => <tr key={record._id}>
                        <td>{record.name}</td>
                        <td>{moment(record.startDate, 'x').format("DD/MMM/YYYY") + " - " + moment(record.endDate, 'x').format("DD/MMM/YYYY")}</td>
                        <td>{getDays(record.startDate, record.endDate)}</td>
                        <td>{record.location}</td>
                    </tr>)}
            </tbody>
        </table>
        : null}
    </div>
}

export default Dates;