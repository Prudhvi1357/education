import React, { useEffect, useState, Fragment } from "react";

const Curriculum = (props) => {

    return <section className="curr">
        <div className="container">
            <div id="chapter-carousel" className="owl-carousel">
                <div className="item">
                    <h4>Chapter 1</h4>
                </div>
                <div className="item">
                    <h4>Chapter 2</h4>
                </div>
                <div className="item">
                    <h4>Chapter 3</h4>
                </div>
                <div className="item">
                    <h4>Chapter 4</h4>
                </div>
                <div className="item">
                    <h4>Chapter 5</h4>
                </div>
                <div className="item">
                    <h4>Chapter 6</h4>
                </div>
                <div className="item">
                    <h4>Chapter 7</h4>
                </div>
                <div className="item">
                    <h4>Chapter 8</h4>
                </div>
                <div className="item">
                    <h4>Chapter 9</h4>
                </div>
                <div className="item">
                    <h4>Chapter 10</h4>
                </div>
                <div className="item">
                    <h4>Chapter 11</h4>
                </div>
                <div className="item">
                    <h4>Chapter 12</h4>
                </div>
            </div>

            <div className="row offline-sec">
                <div className="col-lg-8">
                    <div className="description-part">
                        <h4>Description</h4>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                        <hr />
                        <h4>Notes</h4>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                        <hr />
                        <h4>Assignment</h4>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                        <a href="#" className="status off">Offline</a>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div className="card-part">
                        <img className="video-img" src="/images/detail-video.png" width="100%" />
                        <div className="resource-card">
                            <h4>Resource</h4>
                            {/* <!-- <audio controls> -->
      <!-- <source src="horse.ogg" type="audio/ogg"> -->
      <!-- <source src="horse.mp3" type="audio/mpeg"> -->
      <!-- Your browser does not support the audio tag. -->
     <!-- </audio> -->*/}
                            <p><img src="/images/icons/music.png" /> &nbsp; file name.mp3</p>
                            <p><img style={{ position: "relative", top: "4px" }} src="/images/icons/mp3.png" /></p>
                            <p className="mt-10"><img src="/images/icons/pdf.png" /> &nbsp; file name.pdf</p>
                            <p className="mt-10"><img src="/images/icons/play.png" /> &nbsp; file name.mp4</p>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row online-sec" style={{ display: "none" }}>
                <div className="col-lg-8">
                    <div className="description-part">
                        <h4>Description</h4>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                        <hr />
                        <h4>Notes</h4>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                        <hr />
                        <h4>Assignment</h4>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                        <a href="#" className="status on">Online</a>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div className="card-part">
                        <img className="video-img" src="/images/detail-video2.png" style={{ boxShadow: "0 0 9px #ddd" }} width="100%" />

                        <div className="resource-card golive">
                            <h4>Time to go live</h4>
                            <div className="flexible">
                                <div className="time-box">
                                    <p>23</p>
                                    <h6>Days</h6>
                                </div>
                                <div className="time-box">
                                    <p>12</p>
                                    <h6>Hours</h6>
                                </div>
                                <div className="time-box">
                                    <p>23</p>
                                    <h6>Minutes</h6>
                                </div>
                                <div className="time-box">
                                    <p>54</p>
                                    <h6>Seconds</h6>
                                </div>
                            </div>
                            <hr />
                            <div className="flexible">
                                <div className="time-box">
                                    <h5 style={{ fontSize: "15px", padding: "6px 0 0 8px" }}><a href="#">Notify Me</a></h5>
                                </div>
                                <div className="time-box">
                                    <h5 style={{ fontSize: "15px", padding: "6px 0 0 44px", borderLeft: "1px solid #ddd" }}><a href="#">Add to calender</a></h5>
                                </div>
                            </div>
                        </div>

                        <div className="resource-card golive" style={{ padding: "17px 20px 0 20px" }}>
                            <h4>Timeing</h4>

                            <div className="flexible">
                                <div className="time-box">
                                    <h4><img src="/images/icons/calender.png" /> 06/04/2020</h4>
                                </div>
                                <div className="time-box">
                                    <h4>
                                        <img src="/images/icons/clock.png" /> 03:15 PM - 06:15 PM</h4>
                                </div>
                            </div>
                        </div>

                        <div className="resource-card">
                            <h4>Resource</h4>
                            {/*<!-- <audio controls> -->
      <!-- <source src="horse.ogg" type="audio/ogg"> -->
      <!-- <source src="horse.mp3" type="audio/mpeg"> -->
      <!-- Your browser does not support the audio tag. -->
          <!-- </audio> -->*/}
                            <p><img src="/images/icons/music.png" /> &nbsp; file name.mp3</p>
                            <p><img style={{ position: "relative", top: "4px" }} src="/images/icons/mp3.png" /></p>
                            <p className="mt-10"><img src="/images/icons/pdf.png" /> &nbsp; file name.pdf</p>
                            <p className="mt-10"><img src="/images/icons/play.png" /> &nbsp; file name.mp4</p>
                        </div>

                    </div>
                </div>
            </div>

            <div className="row venue-sec" style={{ display: "none" }}>
                <div className="col-lg-8">
                    <div className="description-part">
                        <h4>Description</h4>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                        <hr />
                        <h4>Notes</h4>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                        <hr />
                        <h4>Assignment</h4>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                        <a href="#" className="status venue">Event</a>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div className="card-part">
                        <img className="video-img" src="/images/venue-img.png" style={{ boxShadow: "0 0 9px #ddd" }} width="100%" />

                        <div className="resource-card golive">
                            <h4>Event Venue</h4>
                            <p><i className="fa fa-map-marker"></i>DLF phase-2 main road opp rapid metro pillar no. 33-34 Gurugram</p>
                        </div>

                        <div className="resource-card golive">
                            <h4>Event Timing</h4>

                            <div className="flexible">
                                <div className="time-box">
                                    <h4 style={{ margin: 0 }}><img src="/images/icons/calender.png" /> 06/04/2020</h4>
                                </div>
                                <div className="time-box">
                                    <h4 style={{ margin: 0 }}>
                                        <img src="/images/icons/clock.png" /> 03:15 PM - 06:15 PM</h4>
                                </div>
                            </div>
                            <div className="flexible">
                                <div className="time-box">
                                    <h4 style={{ margin: 0 }}><img src="/images/icons/calender.png" /> 06/04/2020</h4>
                                </div>
                                <div className="time-box">
                                    <h4 style={{ margin: 0 }}>
                                        <img src="/images/icons/clock.png" /> 03:15 PM - 06:15 PM</h4>
                                </div>
                            </div>
                            <div className="flexible">
                                <div className="time-box">
                                    <h4 style={{ margin: 0 }}><img src="/images/icons/calender.png" /> 06/04/2020</h4>
                                </div>
                                <div className="time-box">
                                    <h4 style={{ margin: 0 }}>
                                        <img src="/images/icons/clock.png" /> 03:15 PM - 06:15 PM</h4>
                                </div>
                            </div>
                            <hr />
                            <p className="center"><a href="#" style={{ color: "#666", textDecoration: "none" }}>Add to Calender</a></p>
                        </div>

                        <div className="resource-card">
                            <h4>Resource</h4>
                            {/*<!-- <audio controls> -->
      <!-- <source src="horse.ogg" type="audio/ogg"> -->
      <!-- <source src="horse.mp3" type="audio/mpeg"> -->
      <!-- Your browser does not support the audio tag. -->
      <!-- </audio> -->*/}
                            <p><img src="/images/icons/music.png" /> &nbsp; file name.mp3</p>
                            <p><img style={{ position: "relative", top: "4px" }} src="/images/icons/mp3.png" /></p>
                            <p className="mt-10"><img src="/images/icons/pdf.png" /> &nbsp; file name.pdf</p>
                            <p className="mt-10"><img src="/images/icons/play.png" /> &nbsp; file name.mp4</p>
                        </div>

                    </div>
                </div>
            </div>

            <br />
            <hr />
            
        </div>
    </section>
}

export default Curriculum;