import React, { useEffect, useState, Fragment } from "react";
import { connect } from 'react-redux'
import '../../../../css/adminpages.css';
import '../../../../css/landingpage.css';
import { getCourse } from "../../../actions/course-actions";
import Loading from "../../../common/loading";
import moment from "moment";
import CourseViewDetail from "./courseviewdetail";

const ViewCourse = (props) => {
  const [loading, setLoading] = useState(true);
  let params = props.match.params;

  useEffect(() => {
    if (params && params.id) {
      props.getCourse(params.id).then(res => {
        setLoading(false)
        console.log("res === ", res);
      }).catch(err => {
        setLoading(false)
        console.log("err === ", err);
      })
    }
  }, [props.match.params])

  const getDays = (end) => {
    let _s = moment();
    let _e = moment(end, 'x');
    let d = _e.diff(_s, 'days');
    console.log("d === ", d);
    return d > 0 ? d + " Days" : 1 + " Day";
  }

  const getPrice = () => {
    const { course } = props;
    if (course && course.pricing) {
      const { pricing } = course;
      let d = new Date().valueOf()
      if (pricing.free) {
        return pricing.freeUntil && pricing.freeUntil < d ?
          <h3>FREE</h3>
          : <Fragment>
            <h3>FREE</h3>
            <p><i className="fa fa-clock-o"></i> {getDays(pricing.freeUntil)} left</p>
          </Fragment>;
      } else if (pricing.discount) {
        return pricing.discountUntil && pricing.discountUntil < d ?
          <Fragment>
            <h3><i className="fa fa-inr"></i> {pricing.discountedPrice} <small style={{ textDecoration: "line-through" }}><i className="fa fa-inr"></i> {pricing.price} &nbsp; {((pricing.price - pricing.discountedPrice) * 100) / pricing.price}% Off</small></h3>
            <p><i className="fa fa-clock-o"></i> {getDays(pricing.discountUntil)} left at this price!</p>
          </Fragment>
          : <h3>{pricing.price}</h3>

      } else {
        return <h3><i className="fa fa-inr"></i> {pricing.price}</h3>;
      }
    }
  }

  if (!loading) {
    const { course } = props;
    return <Fragment>
      <section className="categories-detail">
        <div className="container">
          <div className="row">
            <div className="col-lg-3">
              <img src={course && course.thumbnail ? course.thumbnail : "/images/category-detail.png"} width="100%" />
            </div>
            <div className="col-lg-7">
              <div className="cd-description">
                <h1>{course && course.title ? course.title : ""}</h1>
                <div className="flexible">
                  <div className="dd-box">
                    <h6>For</h6>
                    <h5>{course.targetUser ? course.targetUser : ''}{course && course.targetSubUser ? `, ${course.targetSubUser.join()}` : ''}</h5>
                  </div>
                  <div className="dd-box">
                    <h6>Board</h6>
                    <h5>{course && course.board ? course.board : ""}</h5>
                  </div>
                  <div className="dd-box">
                    <h6>Age-level</h6>
                    <h5>{course && course.ageLevel ? `${course.ageLevel[0]} Years` : ""}</h5>
                  </div>
                  <div className="dd-box">
                    <h6>Category</h6>
                    <h5>{course && course.category ? course.category.join() : ""}</h5>
                  </div>
                  <div className="dd-box">
                    <h6>Subject</h6>
                    <h5>Social</h5>
                  </div>
                </div>
                <br /><br />
                <div className="flexible">
                  <div className="dd-box2" style={{ marginTop: "-10px" }}>
                    {getPrice()}
                  </div>
                  <div className="dd-box2">
                    <button className="addtocart2">Add to cart</button>
                  </div>
                  <div className="dd-box2">
                    <button className="buynow2">Buy now</button>
                  </div>
                  <div className="dd-box2">
                    <button className="sharebtn2"><i className="fa fa-share"></i> <br /> Share</button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-2 bl">
              <h4 className="fm-head">Faculty Members</h4>
              <div className="faculty-member-box">
                <img src="/images/icons/faculty-member.png" />
                <h5>Ayush sharma</h5>
                <small>Principle</small>
              </div>
              <div className="faculty-member-box">
                <img src="/images/icons/faculty-member.png" />
                <h5>Ayush sharma</h5>
                <small>Principle</small>
              </div>

              <p className="seemore"><a href="#">See More</a></p>
            </div>
          </div>
        </div>
      </section>
      <CourseViewDetail course={course}/>
    </Fragment>
  } else {
    return <Loading />
  }

}

const mapStateToProps = (state) => {
  return { course: state.admincourse.courseData };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCourse: (id) => (dispatch(getCourse(id)))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewCourse);