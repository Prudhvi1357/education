import React from "react";

const Faculty = (props) => {

    return <div className="no-pad" data-tab="#faculty">
    <div className="anything">
        <div className="faculty-detail faculty" id="faculty-detail">
            <div className="row">
                <div className="col-lg-2">
                    {/*<div className="faculty-img" contenteditable="false">*/}
                    <div className="faculty-img">
                        <img src="/images/faculty-detail.png" width="100%" />
                        <span className="upload-icon"><img src="/images/icons/upload-img.png" /></span>
                    </div>
                </div>
                <div className="col-lg-10">
                    <div className="content-faculty">
                        <h2 style={{ fontSize: "24px" }} className="aftclc">John snow</h2>
                        <div className="flexible">
                            <div className="cf-box">
                                <h5 className="aftclc">Qualification</h5>
                                <h4 className="aftclc">Phd</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">Experience</h5>
                                <h4 className="aftclc">13</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">University</h5>
                                <h4 className="aftclc">University of oxford</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">Designation</h5>
                                <h4 className="aftclc">Professor</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">Area of intrest</h5>
                                <h4 className="aftclc">Material science</h4>
                            </div>
                        </div>
                    </div>
                    <div className="cf-about">
                        <h4 className="aftclc">About</h4>
                        <p className="aftclc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>

        <hr />
        <div className="faculty-detail faculty" id="faculty-detail">
            <div className="row">
                <div className="col-lg-2">
                    {/*<div className="faculty-img" contenteditable="false">*/}
                    <div className="faculty-img">
                        <img src="/images/faculty-detail.png" width="100%" />
                        <span className="upload-icon"><img src="/images/icons/upload-img.png" /></span>
                    </div>
                </div>
                <div className="col-lg-10">
                    <div className="content-faculty">
                        <h2 style={{ fontSize: "24px" }} className="aftclc">John snow</h2>
                        <div className="flexible">
                            <div className="cf-box">
                                <h5 className="aftclc">Qualification</h5>
                                <h4 className="aftclc">Phd</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">Experience</h5>
                                <h4 className="aftclc">13</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">University</h5>
                                <h4 className="aftclc">University of oxford</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">Designation</h5>
                                <h4 className="aftclc">Professor</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">Area of intrest</h5>
                                <h4 className="aftclc">Material science</h4>
                            </div>
                        </div>
                    </div>
                    <div className="cf-about">
                        <h4 className="aftclc">About</h4>
                        <p className="aftclc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>

        <hr />
        <div className="faculty-detail faculty" id="faculty-detail">
            <div className="row">
                <div className="col-lg-2">
                    {/*<div className="faculty-img" contenteditable="false">*/}
                    <div className="faculty-img" >
                        <img src="/images/faculty-detail.png" width="100%" />
                        <span className="upload-icon"><img src="/images/icons/upload-img.png" /></span>
                    </div>
                </div>
                <div className="col-lg-10">
                    <div className="content-faculty">
                        <h2 style={{ fontSize: "24px" }} className="aftclc">John snow</h2>
                        <div className="flexible">
                            <div className="cf-box">
                                <h5 className="aftclc">Qualification</h5>
                                <h4 className="aftclc">Phd</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">Experience</h5>
                                <h4 className="aftclc">13</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">University</h5>
                                <h4 className="aftclc">University of oxford</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">Designation</h5>
                                <h4 className="aftclc">Professor</h4>
                            </div>
                            <div className="cf-box">
                                <h5 className="aftclc">Area of intrest</h5>
                                <h4 className="aftclc">Material science</h4>
                            </div>
                        </div>
                    </div>
                    <div className="cf-about">
                        <h4 className="aftclc">About</h4>
                        <p className="aftclc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>   
</div>
}

export default Faculty;