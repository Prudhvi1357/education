import React,{useEffect,useState} from "react";
import { useHistory } from "react-router-dom";
import RelatedCourses from "../components/relatedcourses";
import Reviews from './components/reviews';
import Faculties from '../components/faculty';
import '../../../css/adminpages.css';
import '../../../css/landingpage.css';
import {getCourses} from './../../actions/course-actions';
import category from "../../category";
const initialFilter = {
    "free": false,
    "paid": false,
    "targetUser": [],
    "targetSubUser": [],
    "category": [],
    "subCategory": []
  }

export default (props) => {

    const [courses,setCourses] = useState([]);
    const [selectedPage,setSelectedPage] = useState(1);
    const [filters,setFilters] = useState(initialFilter);
    const [hoverActive,setHoverState] = useState(0);
    let History = useHistory();

    const fetchCourses = (offset,limit,body)=>{
        getCourses(offset,limit,body).then((response)=>{
            // console.log(response.data);
           setCourses(response);
         }).catch(()=> console.log("error"));
    }
    useEffect(()=>{
        fetchCourses(0,9,filters);
    },[selectedPage,filters]);

    const pageCount = ()=>{
        return (new Array(Math.ceil(courses.count/9))).fill(0);
    }
    const setFilter = (arrayData , value ,keyName)=>{
        debugger;
        let newArray;
        let newState;
        let newSelection;
        if(arrayData.indexOf(value)>-1 ){
            newSelection = arrayData.filter((user)=>{
                return user !== value
            })
        
       }
       else{
        newSelection= [...arrayData,value];
       }
       if(keyName === 'targetUser'){
        newState = {...filters,targetUser:newSelection};
       }
       else if(keyName === 'targetSubUser'){
        newState = {...filters,targetSubUser:newSelection};
       }
       else if(keyName === 'category'){
        newState = {...filters,category:newSelection};
       }
       else if(keyName === 'subCategory'){
        newState = {...filters,subCategory:newSelection};
       }
       console.log(newState);
       setFilters(newState);
    }

    const getFilters = ()=>{
        return <div className="col-lg-3">
        <div className="course-level">
            <h4>Course For</h4>
            <ul>{
                ['Teacher','Principle','Own manager'].map((courseFor)=>{
                    return <li><a className={`${filters.targetUser.indexOf(courseFor)>-1?'active':''}`} onClick={(e)=>{
                            e.preventDefault();
                            setFilter(filters.targetUser,courseFor,'targetUser')
                       }}>{courseFor}</a></li>
                })
                }
            </ul>
        </div>
        <div className="allcourse course-level">
            <h4>Courses Type</h4>
            <ul>
                <li><a className={`${filters.free?'active':''}`} href="#"  onClick={(e)=>{setFilters({...filters,free:!filters.free})}}>Free</a></li>
                <li><a className={`${filters.paid?'active':''}`} href="#" onClick={(e)=>{setFilters({...filters,paid:!filters.paid})}}>Paid</a></li>
            </ul>
        </div>
        <div className="allcourse course-level">
            <h4>Courses Level</h4>
            <ul>
                {['Pharmacy 1','Pharmacy 2','Pharmacy 3'].map((level)=>{
                    return <li><a className={`${filters.subCategory.indexOf(level)>-1?'active':''}`} href="#" onClick={(e)=>{
                        e.preventDefault();
                        setFilter(filters.subCategory,level,'subCategory')
                   }}>{level}</a></li>
                })}
               
            </ul>
        </div>
        <div className="allcourse course-level">
            <h4>All Courses</h4>
            <ul>
            
            {['Social Science','Mathematics','Geography','Science'].map((category) => {
                  return (<li><a className={`${(filters.category).indexOf(category)>-1 ? 'active':''}`} href="#" onClick={(e)=>{
                    e.preventDefault();
                   setFilter(filters.category,category,'category')
                   }}>{category}</a></li>)
            })}
               
            </ul>
        </div>
    </div>
    }

    const isSelected = (id)=>{
        return id  === hoverActive;
    }

    const drawCourseCards = ()=>{
         return (<div className="col-lg-9">
             <div className="course-list">
                 <h1>Political Science &nbsp;  <span>Middle &nbsp; <i className="fa fa-close"></i></span> &nbsp; <span>Junior &nbsp; <i className="fa fa-close"></i></span></h1>
                 <div className="row">
                     {courses.data && courses.data.map((course,i)=>{
                     return <div key={i} className="col-lg-4 toadd">
                         <div className="course-card spe" onMouseEnter={(e)=>{
                             console.log("hovering",course._id)
                             if(hoverActive !== course._id){
                                setHoverState(course._id)
                             }
                             }} onMouseLeave={(e)=>{setHoverState(0);}}>
                             <div className="card-img">
                                <span> <img src='images/course1.png' width={`${isSelected(course._id)?'50%':'100%'}`} />{isSelected(course._id) && course.title}</span>
                                 <div className="card-overlay">
                                     <span className="strip">
                                         {course.targetUser}
                                     </span>
                                     {course.targetSubUser && course.targetSubUser.map((user)=>{
                                     return (<span className="strip" style={{marginLeft:'10px'}}>
                                          {user}
                                     </span>)
                                     })
                                     }
                                     <p><button className="detailbtn" onClick={()=>{History.push(`/courses/${course._id}`)}} >View Detail</button></p>
                                 </div>
                             </div>
                             <div className="course-card-body">
                                 <button className="basicbtn">Social</button>
                                 <div className="temp12">
                                 {!isSelected(course._id) && <h4>{course.title}</h4>}
                                    {!isSelected(course._id) && <p>{course.description} <a href="#" className="rm">Read More</a></p>}
                                    {isSelected(course._id) && <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>}
                                    {isSelected(course._id) &&<div className="button-group">
                                         {
                                             course.metaKeyword.map((keyWord)=>{
                                             return <button className="bkt-btn">{keyWord}</button>
                                             })
                                         }
        
                                     </div>
                                    }
                                 </div>
                                 <hr />
                                 <div className="temp22">
                                        {!course.pricing.free && <h3><i className="fa fa-inr"></i> {course.pricing.discountedPrice} <small style={{ textDecoration: "line-through", color: "#999" }}><i className="fa fa-inr"></i> {course.pricing.price}</small><span className="wishing"><i className="fa fa-heart"></i></span></h3>}
                                        {course.pricing.free && <h3>Free till {course.pricing.freeUntil}</h3>}
                                     {!course.pricing.free && <p style={{ color: "#a90c17", fontWeight: 500 }}><i className="fa fa-clock-o"></i> {course.pricing.discountUntil} days left at this price!</p>}
                                         
                                 </div>
                                
                                 {isSelected(course._id) && <div className="row">
                                     <div className="col-lg-6">
                                         <button className="buy-add-btn">Buy now</button>
                                     </div>
                                     <div className="col-lg-6">
                                         <button className="buy-add-btn">Add to cart</button>
                                     </div>
                                 </div>}
                             </div>
                         </div>

                     </div>
                     })
                    }


                 </div>
             </div>
             {courses.count > 9 && <div className="pagination-cus">
                 <div className="row">
                     <div className="col-lg-3">
                         <p>Page : &nbsp; 1 of {pageCount().length}</p>
                     </div>
                     <div className="col-lg-7">
                         <p>
                         {pageCount().map((item,i)=>{
                            return <span onClick={(e)=>{setSelectedPage(i+1)}} className={`${selectedPage===(i+1)?'active':''}`}>{i+1}</span>
                         })}
                         </p>
                                     </div>
                     <div className="col-lg-2 text-right">
                         <button>Next</button>
                     </div>
                 </div>
             </div>
            }
         </div>)
    }
    return <section className="categories">
        <div className="container">
            <div className="row">
               {getFilters()}
               {drawCourseCards()}
            </div>
        </div>
    </section>
}