import React from "react";

export default (props) => {

    return <div className="faculty-c">
        <h3>Faculty</h3>
        <div className="row">
            <div className="col-lg-3">
                <div className="faculty-c-box">
                    <p><img src="images/icons/f1.png" alt="" /></p>
                    <h4>Ajnu Mehrotra</h4>
                    <p>Associate Professor</p>
                </div>
            </div>
            <div className="col-lg-3">
                <div className="faculty-c-box">
                    <p><img src="images/icons/f2.png" alt="" /></p>
                    <h4>Deepika Thapar</h4>
                    <p>Associate Professor</p>
                </div>
            </div>
            <div className="col-lg-3">
                <div className="faculty-c-box">
                    <p><img src="images/icons/f3.png" alt="" /></p>
                    <h4>Mayank Sharma</h4>
                    <p>Associate Professor</p>
                </div>
            </div>
            <div className="col-lg-3">
                <div className="faculty-c-box">
                    <p><img src="images/icons/f4.png" alt="" /></p>
                    <h4>Sudhanshu</h4>
                    <p>Associate Professor</p>
                </div>
            </div>
        </div>
    </div>

}