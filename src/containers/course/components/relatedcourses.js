import React from "react";

export default (props) => {

    return <div className="related-course">
        <h3>Related Courses</h3>

        <div className="row">
            <div className="col-lg-3">
                <div className="course-card">
                    <div className="card-img">
                        <img src="images/course1.png" alt="course" />
                        <div className="card-overlay">
                            <div className="strip">
                                Senior Section
          </div>
                            <p><button className="detailbtn">View Detail</button></p>
                        </div>
                    </div>
                    <div className="course-card-body">
                        <button className="basicbtn">Social</button>
                        <h4>Political Science</h4>
                        <p>The lysine contingency it’s intended to prevent the <a href="#">Read More</a></p>
                        <hr />
                        <div className="row">
                            <div className="col-lg-6">
                                <p><i className="fa fa-user"></i> 22 &nbsp; &nbsp; <i className="fa fa-star"></i> 5</p>
                                <small>For Teachers</small>
                            </div>
                            <div className="col-lg-6">
                                <button className="inr"><i className="fa fa-inr"></i> 599</button>
                                <button className="addtocart">Add To Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-lg-3">
                <div className="course-card">
                    <div className="card-img">
                        <img src="images/course2.png" alt="course" />
                        <div className="card-overlay">
                            <div className="strip">
                                Senior Section
          </div>
                            <p><button className="detailbtn">View Detail</button></p>
                        </div>
                    </div>
                    <div className="course-card-body">
                        <button className="basicbtn">Lifestyle</button>
                        <h4>Political Science</h4>
                        <p>The lysine contingency it’s intended to prevent the <a href="#">Read More</a></p>
                        <hr />
                        <div className="row">
                            <div className="col-lg-6">
                                <p><i className="fa fa-user"></i> 22 &nbsp; &nbsp; <i className="fa fa-star"></i> 5</p>
                                <small>For Teachers</small>
                            </div>
                            <div className="col-lg-6">
                                <button className="inr"><i className="fa fa-inr"></i> 599</button>
                                <button className="addtocart">Add To Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-lg-3">
                <div className="course-card">
                    <div className="card-img">
                        <img src="images/course3.png" alt="course" />
                        <div className="card-overlay">
                            <div className="strip">
                                Senior Section
          </div>
                            <p><button className="detailbtn">View Detail</button></p>
                        </div>
                    </div>
                    <div className="course-card-body">
                        <button className="basicbtn">Marketing</button>
                        <h4>Political Science</h4>
                        <p>The lysine contingency it’s intended to prevent the <a href="#">Read More</a></p>
                        <hr />
                        <div className="row">
                            <div className="col-lg-6">
                                <p><i className="fa fa-user"></i> 22 &nbsp; &nbsp; <i className="fa fa-star"></i> 5</p>
                                <small>For Teachers</small>
                            </div>
                            <div className="col-lg-6">
                                <button className="inr"><i className="fa fa-inr"></i> 599</button>
                                <button className="addtocart">Add To Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="col-lg-3">
                <div className="course-card">
                    <div className="card-img">
                        <img src="images/course1.png" alt="" />
                        <div className="card-overlay">
                            <div className="strip">
                                Senior Section
          </div>
                            <p><button className="detailbtn">View Detail</button></p>
                        </div>
                    </div>
                    <div className="course-card-body">
                        <button className="basicbtn">Social</button>
                        <h4>Political Science</h4>
                        <p>The lysine contingency it’s intended to prevent the <a href="#">Read More</a></p>
                        <hr />
                        <div className="row">
                            <div className="col-lg-6">
                                <p><i className="fa fa-user"></i> 22 &nbsp; &nbsp; <i className="fa fa-star"></i> 5</p>
                                <small>For Teachers</small>
                            </div>
                            <div className="col-lg-6">
                                <button className="inr"><i className="fa fa-inr"></i> 599</button>
                                <button className="addtocart">Add To Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

}