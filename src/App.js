import React, { createContext } from 'react';
import './App.css';
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from 'react-redux'
import Routes from "./Routes";
import store from "./store";
// export const AppContext = createContext(null);

export default function App() {
  
  return (
    <Provider store={store}>
    <Router>
      <div className="App">
        {/* <AppContext.Provider value={{ userData, setUser }}> */}
          <Routes childProps={{}} />
        {/* </AppContext.Provider> */}
      </div>
    </Router>
    </Provider>

  );
}