import React, { Suspense, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";
import Home from '../layouts/Home/index';

export default ({ component: C, props: cProps, ...rest }) => {
    let token = localStorage.getItem("userAuthToken");
    let role = localStorage.getItem("userRole");
    console.log("role ==== ", role)
    // console.log("Root Action", cProps, rest);
    // function redirectActions(params) {
    //     if (params.path === "/") {
    //         switch(params.userData.role) {
    //             case "admin" : return "/admin/category";
    //             case "teacher": return "/admin/category";
    //             case "user": return "/admin/category";
    //             default: return "/admin/category";
    //         }
    //     } else {
    //         return params.path;
    //     }
    // }


    return (
        <Route
            {...rest}
            render={props => <Suspense fallback={<div>Loading... </div>}>
                {/* {token && token !== '' ? <Redirect to="/" /> : <Home {...props} {...cProps} component={(props) => <C  {...cProps} {...props} />} />} */}
                <Home {...props} {...cProps} component={(props) => <C  {...cProps} {...props} />} />
            </Suspense>}
        />
    );
};
