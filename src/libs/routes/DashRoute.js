import React, { Suspense, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";

export default ({ component: C, layout: Layout, props: cProps, ...rest }) => {
    
    let token = localStorage.getItem("userAuthToken");
    
    return (
        <Route
            {...rest}
            render={props => <Suspense fallback={<div>Loading... </div>}>
                <Layout {...props} {...cProps} component={(props) => <C  {...cProps} {...props} />} />
            </Suspense>}
        />
    );
};

// {token && token !== '' ? <Layout {...props} {...cProps} component={(props) => <C  {...cProps} {...props} />} /> :
// <Redirect to="/" />}
