import React, {Suspense} from "react";
import { Route, Redirect } from "react-router-dom";


export default  ({ component: C, props: cProps, ...rest }) => {
    console.log("Root Action", cProps, rest);
    const token = localStorage.getItem('userAuthToken');
    return (
    //   <Route
    //     {...rest}
    //     render={props =>
    //       !cProps.isAuthenticated
    //     ? <Home {...props} {...cProps} component={(props) => <C  {...cProps} {...props} /> } />
    //         : <Redirect
    //             to={redirect === "" || redirect === null ? validateSetup(cProps.userPayload[mcu_key]) : validateActions(redirect, cProps)}
    //           />}
    //   />
    <Route
        {...rest}
        render={props => <Suspense fallback={<div>Loading... </div>}>
                                {token && token !== '' ? <C  {...cProps} {...props} /> : <Redirect to="/" />}
                        </Suspense>}
      />
    );
  };