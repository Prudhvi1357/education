import React, { Suspense, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";
import AdminLayout from "../layouts/Admin/index";

export default ({ component: Component, ...rest }) => {

    let token = localStorage.getItem("userAuthToken");
    let role = localStorage.getItem("userRole");

    return (
        <Route
            {...rest}
            render={matchProps => <Suspense fallback={<div>Loading... </div>}>
                {token && token !== '' ?
                    role && role === "superadmin" ?
                        <AdminLayout>
                            <Component  {...matchProps} />
                        </AdminLayout>
                        : <Redirect to="/dashboard" />
                    : <Redirect to="/" />}
            </Suspense>}
        />
    );
};
