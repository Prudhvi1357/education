export const validateCharecters = function (data) {
  var re = /^([a-zA-Z ]+)$/;
  return re.test(data);
};

export const validateName = function (data) {
  var re = /^([a-zA-Z. ]+)$/;
  return re.test(data);
};

export const validateIntegers = function (data) {
  var re = /^([0-9]+)$/;
  return re.test(data);
};

export const validateEmail = function (data) {
  // var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  var re = /^((([a-zA-Z]|[0-9])|([-]|[_]|[.])){2,})+[@](([a-zA-Z0-9])|([-]|[.])){2,40}[.]((([a-zA-Z0-9]){2,4})|(([a-zA-Z0-9]){2,4}[.]([a-zA-Z0-9]){2,4}))$/;
  return re.test(data);
};


export const validatePassword = (data) => {
  var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/;
  return regex.test(data)
}

export const validatePincode = (data) => {
  var re = /^([0-9]{6,10})$/;
  return re.test(data);
}

export const validateWebsite = (data) => {
  var regex = /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}?(\/)?$/;
  return regex.test(data)
}

