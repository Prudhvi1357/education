import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import Header from '../common/header';
import Footer from '../common/footer';

const ViewLayout = (props) => {
    return <Fragment>
        <Header />
        <Route exact path={props.path} component={props.component} />
        <Footer />
    </Fragment>
}

export default ViewLayout;