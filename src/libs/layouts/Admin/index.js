import React, { Fragment, useEffect } from 'react';
import { Route } from 'react-router-dom';
import { connect } from "react-redux";
import Header from "./components/header";
import LeftMenu from "./components/leftmenu";
import { getUserDetails } from "../Home/actions";

const AdminLayout = ({ children, getUserDetails }) => {
    useEffect(() => {
        getUserDetails();
    }, []);

    return <Fragment>
        <Header />
        <div className="desh-content">
            <div className="flexible">
                <LeftMenu />
                {children}
            </div>
        </div>
    </Fragment>
}

const mapDispatchToProps = (dispatch) => {
    return {
        getUserDetails: () => dispatch(getUserDetails()),
    };
};

export default connect(null, mapDispatchToProps)(AdminLayout);