import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import Header from './componets/header';
import SideNav from './componets/sideNav';

const CourseLayout = (props) => {
    return <Fragment>
        <Header />
        <div className="desh-content">
            <div className="flexible">
                <SideNav />
                <Route exact path={props.path} component={props.component} />
            </div>
        </div>
    </Fragment>
}

export default CourseLayout;