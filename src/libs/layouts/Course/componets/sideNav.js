import React from 'react';
import { useLocation } from 'react-router-dom'
import "../../../../css/sidebar.css"

const SideNav = (props) => {
    let location = useLocation();
    console.log(location.pathname);
    return <div className="dash-sidebar">
        <ul>
            <li><a href="#"><i className="fa fa-home"></i> Dashboard</a></li>
            <li><a href="#"><i className="fa fa-list-alt"></i> Categories</a></li>
            <li><a href="#"><i className="fa fa-university"></i> Faculty</a></li>
            <li><a href="#"><i className="fa fa-youtube-play"></i> Courses</a></li>
            <li><a href="#"><i className="fa fa-users"></i> Teachers</a></li>
            <li className="enrolled"><a href="#"><i className="fa fa-sticky-note-o"></i> Enrolment <i className="fa fa-angle-down pull-right" style={{"fontSize": "20px"}}></i></a>
                <ul>
                    <li><a href="#">Enrolment 1</a></li>
                    <li><a href="#">Enrolment 2</a></li>
                </ul>
            </li>
            <li><a href="#"><i className="fa fa-cog"></i> Setting</a></li>
            <li><a href="#"><i className="fa fa-power-off"></i> Sign out</a></li>
        </ul>
    </div>
}


export default SideNav;