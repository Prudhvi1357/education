import React from 'react';
import "../../../../css/header.css"

const Header = (props) => {

    const logout = () => {
        localStorage.clear();
    };
    return <div className="dash-header">
        <div className="flexible">
            <div className="logo-side">
                <img src="images/logo.png" alt="Logo" />
            </div>
            <div className="pro-side">
                <ul>
                    <li><i className="fa fa-search"></i></li>
                    <li><img src="images/profile-pic.png" alt="Profile Pic"  onClick={logout}/></li>
                </ul>
            </div>
        </div>
    </div>
}


export default Header;