import React from 'react';
import "../../../../css/sidebar.css"

const LeftMenu = (props) => {

    return <div className="col-lg-3">
        <div className="course-level">
            <h4>Course Level</h4>
            <ul>
                <li><a href="#">Pre-kindergarten</a></li>
                <li><a href="#">Elementary</a></li>
                <li><a className="active" href="#">Junior</a></li>
                <li><a className="active" href="#">Middle</a></li>
                <li><a href="#">Senior</a></li>
            </ul>
        </div>
        <div className="allcourse">
            <h4>All Courses</h4>
            <ul>
                <li className="question open" id="question">
                    <div className="expand-bar"></div>
                    Social Science
          </li>
                <li className="answer" style={{"display": "list-item;"}}>
                    <ul className="subjects">
                        <li><a className="active" href="#">Political Science</a></li>
                        <li><a href="#">Geography</a></li>
                        <li><a href="#">History</a></li>
                        <li><a href="#">Economics</a></li>
                    </ul>
                </li>
                <li className="question">
                    <div className="expand-bar">
                    </div>
                    Language
          </li>
                <li className="answer">
                    <ul className="subjects">
                        <li><a href="#">Language 1</a></li>
                        <li><a href="#">Language 2</a></li>
                        <li><a href="#">Language 3</a></li>
                        <li><a href="#">Language 4</a></li>
                    </ul>
                </li>
                <li className="question">
                    <div className="expand-bar">
                    </div>
                    Mathematics
          </li>
                <li className="answer">
                    <ul className="subjects">
                        <li><a href="#">Mathematics 1</a></li>
                        <li><a href="#">Mathematics 2</a></li>
                        <li><a href="#">Mathematics 3</a></li>
                        <li><a href="#">Mathematics 4</a></li>
                    </ul>
                </li>
                <li className="question">
                    <div className="expand-bar">
                    </div>
                    Science
          </li>
                <li className="answer">
                    <ul className="subjects">
                        <li><a href="#">Science 1</a></li>
                        <li><a href="#">Science 2</a></li>
                        <li><a href="#">Science 3</a></li>
                        <li><a href="#">Science 4</a></li>
                    </ul>
                </li>
                <li className="question">Art</li>
            </ul>
        </div>
    </div>
}


export default LeftMenu;