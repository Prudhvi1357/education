import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import Header from "../common/header";
import Footer from "../common/footer";
import LeftMenu from "./components/leftmenu";

const DashLayout = (props) => {
    return <Fragment>
        <Header />
        <section className="categories">
            <div className="container">
                <div className="row">
                    <LeftMenu />
                    <Route exact path={props.path} component={props.component} />
                </div>
            </div>
        </section>
        <Footer />
    </Fragment>
}

export default DashLayout;