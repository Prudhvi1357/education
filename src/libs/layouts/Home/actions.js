import { callLoginApi, callSignupApi, callForgotPassApi, callVerifyNumberApi, callVerifyOTPApi, callUserDetailsApi } from './services';

export const USER_DETAILS = "USER_DETAILS";

export function loginAction(loginData) {
    return new Promise((resolve, reject) => {
        callLoginApi(loginData, (response) => {
            if (response.status) {
                localStorage.setItem('userAuthToken', response.userAuthToken);
                resolve(response);
            } else {
                reject(response);
                localStorage.clear();
            }
        });
    });
}

export function signupAction(userData) {
    return new Promise((resolve, reject) => {
        callSignupApi(userData, (response) => {
            console.log("response ==== ", response);
            response.status ? resolve(response) : reject(response)
        });
    });
}

export function ForgotPasswordAction(email) {
    return new Promise((resolve, reject) => {
        callForgotPassApi(email, (response) => {
            response.status ? resolve(response) : reject(response)
        });
    });
}

export function VerifyNumberAction(mobile) {
    return new Promise((resolve, reject) => {
        callVerifyNumberApi(mobile, (response) => {
            response.status ? resolve(response) : reject(response)
        });
    });
}

export function VerifyOTPAction(obj) {
    return new Promise((resolve, reject) => {
        callVerifyOTPApi(obj, (response) => {
            if (response.status) {
                localStorage.setItem('userAuthToken', response.userAuthToken);
                resolve(response)
            } else {
                reject(response)
            }
        });
    });
}

export function getUserDetails() {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            return callUserDetailsApi((response) => {
                if (response._id) {
                    dispatch(saveUser(response))
                    resolve(response)
                    return;
                } else {
                    reject(response)
                    return;
                }
            })
        });
    }
}

export function saveUser(data) {
    return {
        type: USER_DETAILS,
        data: data
    }
}
