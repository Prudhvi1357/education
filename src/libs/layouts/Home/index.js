import React, { useEffect, Fragment } from 'react';
import { Route } from 'react-router-dom';
import { connect } from "react-redux";
import Header from './components/header';
import Footer from '../common/footer';
import UserHeader from "../common/header";
import {getUserDetails} from "./actions";

const HomeLayout = (props) => {

    useEffect(() => {
        props
        .getUserDetails()
        .then((response) => {
          console.log(response);
          localStorage.setItem("userRole", response.role);
        }).catch(err => {
            console.log("err in home layout === ", err);
        })
    }, [])

    let token = localStorage.getItem("userAuthToken");

    return <Fragment>
        {props.userDetails && token ? <UserHeader {...props} />
        : <Header {...props} />}
        <Route exact path={props.path} component={props.component} />
        <Footer />
        {/* <a href="javascript:void(0);" id="rocketmeluncur" className="showrocket">
            <i></i>
        </a> */}
    </Fragment>
}

const mapStateToProps = (state) => {
    return { userDetails: state.app.userData };
}

const mapDispatchToProps = (dispatch) => {
    return {
      getUserDetails: () => dispatch(getUserDetails()),
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(HomeLayout);