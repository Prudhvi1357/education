// Import Actions
import { USER_DETAILS } from './actions';

// Initial State
const initialState = {
  userData: null
};

const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_DETAILS:
      return Object.assign({}, state, {
        userData: action.data,
      });

    default:
      return state;
  }
};

// Export Reducer
export default AppReducer;
