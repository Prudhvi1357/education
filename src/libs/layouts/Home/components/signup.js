import React, { useState, useRef } from "react";
import { useHistory } from "react-router-dom";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {
  validateEmail,
  validateCharecters,
  validatePassword,
} from "../../../validations";
import { signupAction } from "../actions";
import "react-phone-number-input/style.css";
import "./signup.css";
import "../../../../css/modal.css";
import PhoneInput, {
  isValidPhoneNumber,
  parsePhoneNumber,
} from "react-phone-number-input";
import { config } from "../../../../config";
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import { loginAction, getUserDetails } from "../actions";

const Signup = (props) => {
  const [firstname, setfname] = useState("");
  const [lastname, setlname] = useState("");
  const [email, setemail] = useState("");
  const [mobile, setmobile] = useState("");
  const [type, settype] = useState("");
  const [password, setpassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [stype, setSType] = useState('');
  const terms = useRef(null);
  const history = useHistory();

  const handleSubmit = (event) => {
    event.preventDefault();
    const phoneNumber = parsePhoneNumber(mobile);
    if (!validateCharecters(firstname)) {
      alert("Please Enter a Valid First Name");
    } else if (!validateCharecters(lastname)) {
      alert("Please Enter a Valid Last Name");
    } else if (!validateEmail(email)) {
      alert("Please Enter a Valid Email");
    } else if (!isValidPhoneNumber(mobile)) {
      alert("Please Enter a Valid Phone Number");
    } else if (!validatePassword(password)) {
      alert("Password length should be more than 6, should contains atleast one Capital Letter, one Small Letter, one Nummeric Value and one Special Character");
    } else if (type === "") {
      alert("Please Select User Type");
    } else if (!terms.current.checked) {
      alert("Please Select the Terms and Conditions");
    } else {

      let obj = {
        firstName: firstname,
        lastName: lastname,
        email: email,
        mobileNumber: phoneNumber.nationalNumber,
        countryCode: "+" + phoneNumber.countryCallingCode,
        role: type,
        password: password,
      };
      handleSignup(obj);
    }
  };

  const handleSignup = (obj) => {
    setLoading(true);
    signupAction(obj)
      .then((res) => {
        console.log("res === ", res);
        if (res.status) {
          alert("Your account has been created, Please check your email for activating your account");
          props.close();
        } else {
          if (res.result && res.result.code === "SCHEMA_VALIDATION_FAILED") {
            alert("Please enter Valid Data");
          } else {
            alert("Please Try Again");
          }
        }
      })
      .catch((err) => {
        console.log("err === ", err);

        if (err.result && err.result.code === "SCHEMA_VALIDATION_FAILED") {
          alert("Please enter Valid Data");
        } else if (err.result && err.result.message) {
          alert(err.result.message)
        } else {
          alert("Internal Server Error, Please Try Again");
        }
      });
  }

  const handleLoginAction = (obj) => {
    loginAction(obj)
      .then((res) => {
        // console.log("res === ", res);
        if (res.status) {
          getProfileData();
        } else {
          alert("Invalid Credentials");
        }
      })
      .catch((err) => {
        console.log("err === ", err);
        alert(err.result.message);
        // alert("Internal Server Error, Please Try Again");
      });
  };

  const getProfileData = () => {
    props
      .getUserDetails()
      .then((response) => {
        console.log(response);
        localStorage.setItem("userRole", response.role);
        history.push("/admin/dashboard");
      })
      .catch((error) => {
        console.log("error in getuserdetails === ", error);
        alert("Please Try Again");
      });
  };

  const responseGoogle = (response) => {
    console.log(response);
    if (response && response.profileObj && response.profileObj.googleId) {
      // success
      let obj = {
        firstName: response.profileObj.name,
        email: response.profileObj.email,
        googleid: response.profileObj.googleId,
        // mobileNumber: phoneNumber.nationalNumber,
        // countryCode: "+" + phoneNumber.countryCallingCode,
        role: type,
        password: "Education@123",
        isActive: true
      };
      handleSignup(obj);
    } else {
      // error
      console.log("error ==== ", response.error);
      alert("Please Try Again");
    }
  };

  const responseFacebook = (response) => {
    console.log(response);
    if (response && response.id) {
      let obj = {
        firstName: response.name,
        // lastName: lastname,
        email: response.email,
        fbid: response.id,
        // mobileNumber: phoneNumber.nationalNumber,
        // countryCode: "+" + phoneNumber.countryCallingCode,
        role: type,
        password: "Education@123",
        isActive: true
      };
      handleSignup(obj)
    } else {
      alert("Please Try Again");
    }
  };

  const errorResponse = (err) => {

  }

  const componentClicked = () => { };


  return (
    <Modal show={true} onHide={props.close}>
      <Modal.Body>
        <p className="close-modal" data-dismiss="modal" onClick={props.close}>
          x
        </p>
        <div className="modal-body white-popupp">
          <div className="flexible login-wrap">
            <div className="image-part">
              <img src="images/login_bg.jpg" alt="Background Logo" />
              <img className="login-logo" src="images/logo.png" alt="Logo" />
            </div>
            <div className="content-part default">
              <h3 className="mb-28">Welcome to Eduexcellence</h3>
              <form onSubmit={handleSubmit}>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="form-group">
                      <input
                        type="text"
                        onChange={(e) => setfname(e.target.value)}
                        required={true}
                      />
                      <label htmlFor="input" className="control-label">
                        First name*
                    </label>
                      <i className="bar"></i>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="form-group">
                      <input
                        type="text"
                        onChange={(e) => setlname(e.target.value)}
                        required={true}
                      />
                      <label for="input" className="control-label">
                        Last name*
                    </label>
                      <i className="bar"></i>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-lg-6">
                    <div className="form-group">
                      <input
                        type="text"
                        onChange={(e) => setemail(e.target.value)}
                        required={true}
                      />
                      <label for="input" className="control-label">
                        Email*
                    </label>
                      <i className="bar"></i>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="form-group">
                      <PhoneInput
                        // placeholder="Enter phone number"
                        defaultCountry="IN"
                        value={mobile}
                        onChange={setmobile}
                        tabIndex={4}
                      />
                      {/* <input id="mobile" type="text" onChange={(e) => setmobile(e.target.value)} required={true} /> */}
                      <label htmlFor="mobile" className={mobile && mobile.length > 0 ? "control-label label-focus" : "control-label"}>
                        Mobile*
                    </label>
                      <i className={mobile && mobile.length > 0 ? "bar bar-focus" : "bar"}></i>
                    </div>
                  </div>
                </div>

                <div className="iam">
                  <span>I am</span>{" "}
                  <button
                    type="button"
                    className={type === "user" ? "studentbtn" : "teacherbtn"}
                    onClick={(e) => settype("user")}
                  >
                    Student
                </button>{" "}
                  <button
                    type="button"
                    className={type === "teacher" ? "studentbtn" : "teacherbtn"}
                    onClick={(e) => settype("teacher")}
                  >
                    Teacher
                </button>
                </div>
                <div className="form-group">
                  <input
                    type="password"
                    onChange={(e) => setpassword(e.target.value)}
                    required={true}
                  />
                  <label for="input" className="control-label">
                    Password*
                </label>
                  <i className="bar"></i>
                </div>

                {/* <div className="checkbox"> */}
                <label>
                  <input type="checkbox" ref={terms} />
                  <span className="checkbox-material">
                    <span className="check"></span>
                  </span>
                  <span className="check-text">
                    By creating an account you agree to the{" "}
                    <a href="#">Terms of use</a> and our{" "}
                    <a href="#">Privacy policy</a>
                  </span>
                </label>
                {/* </div> */}
                <Button size="lg" block type="submit">
                  Sign up
              </Button>
              </form>
              <div className="login-or">
                <hr className="hr-or" />
                <span className="span-or">or</span>
              </div>

              <div className="row">
                <div className="col-lg-6">
                  {/*<GoogleLogin
                    clientId={config.google_client_id}
                    buttonText="Login with Google"
                    onSuccess={responseGoogle}
                    onFailure={errorResponse}
                    cookiePolicy={"single_host_origin"}
                    className="btn btn-second btn-lg btn-block gf-btn"
                  />*/}

                  <button type="button" class="btn btn-primary btn-lg btn-block gf-btn c-g-btn" data-toggle="modal" data-target="#identify" onClick={() => setSType('google')}>
                    <img src="/images/google_logo.png" />SignUp with Google
                  </button>
                </div>
                <div className="col-lg-6">
                  {/*<FacebookLogin
                    appId={config.facebook_app_id}
                    autoLoad={false}
                    textButton=" Login with Facebook"
                    fields="email"
                    onClick={componentClicked}
                    callback={responseFacebook}
                    cssClass="btn btn-second btn-lg btn-block gf-btn"
                    icon="fa-facebook"
                  />*/}
                  <button type="button" class="btn btn-primary btn-lg btn-block gf-btn c-f-btn" data-toggle="modal" data-target="#identify" onClick={() => setSType('facebook')}>
                    <img src="/images/icons/fb.png" />SignUp with Facebook
  </button>
                </div>
              </div>

              <div id="inline-popups3">
                <p className="member signbtn">
                  Already have an account ? &nbsp;{" "}
                  <a
                    onClick={props.signin}
                    data-toggle="modal"
                    data-target="#basicModal"
                    data-dismiss="modal"
                    className="show-cursor"
                  >
                    Sign in
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>






        <div class="modal iu" id="identify" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body white-popupp">
                <div class="identity-box">
                  <h3>Please Identify Yourself</h3>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="img-container">
                        <p><img src="/images/graduate.svg" width="100"
                          onClick={(e) => settype("user")} style={{ cursor: "pointer" }} /></p>
                        <img src="/images/icons/check-green.png"
                          className={type === "user" ? "identity-right u-select" : "identity-none u-select"}
                        />
                        <h4>I am Student</h4>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="img-container">
                        <p><img src="/images/professor.svg" width="100"
                          onClick={(e) => settype("teacher")} style={{ cursor: "pointer" }} /></p>
                        <img src="/images/icons/check-green.png"
                          className={type === "teacher" ? "identity-right u-select" : "identity-none u-select"} />
                        <h4>I am Educator</h4>
                      </div>
                    </div>
                  </div>
                  <p>
                    {/*<button>Continue</button>*/}
                    {stype === "google" &&
                      <GoogleLogin
                        clientId={config.google_client_id}
                        buttonText="Continue"
                        onSuccess={responseGoogle}
                        onFailure={errorResponse}
                        cookiePolicy={"single_host_origin"}
                        className="c-identity-btn"
                      />
                    }
                    {stype === "facebook" &&
                      <FacebookLogin
                        appId={config.facebook_app_id}
                        autoLoad={false}
                        textButton="Continue"
                        fields="email"
                        onClick={componentClicked}
                        callback={responseFacebook}
                        cssClass="c-identity-btn"
                      // icon="fa-facebook"
                      />
                    }
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/*<div class="modal fade" id="iamModal" style={{
          background: "rgba(0, 0, 0, 0.12)"
        }}>
          <div class="modal-dialog">
            <div class="modal-content  c-modal-600">
              <div class="modal-header">
                <h4 class="modal-title">Sign Up As</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
              </div>
              <div class="modal-body" style={{ padding: "40px 24px 24px 24px" }}>
                <div className="iam">
                  <span>I am</span>{" "}
                  <button
                    type="button"
                    className={type === "user" ? "studentbtn" : "teacherbtn"}
                    onClick={(e) => settype("user")}
                  >
                    Student
            </button>{" "}
                  <button
                    type="button"
                    className={type === "teacher" ? "studentbtn" : "teacherbtn"}
                    onClick={(e) => settype("teacher")}
                  >
                    Teacher
            </button>
                </div>
              </div>
              <div class="modal-footer">
                {stype === "google" &&
                  <GoogleLogin
                    clientId={config.google_client_id}
                    buttonText="Submit"
                    onSuccess={responseGoogle}
                    onFailure={errorResponse}
                    cookiePolicy={"single_host_origin"}
                  // className="btn btn-second btn-lg btn-block gf-btn"
                  />
                }
                {stype === "facebook" &&
                  <FacebookLogin
                    appId={config.facebook_app_id}
                    autoLoad={false}
                    textButton="Submit"
                    fields="email"
                    onClick={componentClicked}
                    callback={responseFacebook}
                    cssClass="btn btn-second btn-lg btn-block gf-btn"
                  // icon="fa-facebook"
                  />
                }
              </div>

            </div>
          </div>
              </div>*/}

      </Modal.Body>
    </Modal>
  );
};

export default Signup;
