import utils from '../../../utils/apiCaller';

export function callLoginApi(loginData, callback) {
	utils.httpRequest('login', 'post', loginData, (response) => {
		callback(response);
	});
}

export function callSignupApi(userData, callback) {
	utils.httpRequest('users/registeruser', 'post', userData, (response) => {
		callback(response);
	});
}

export function callForgotPassApi(email, callback) {
	utils.httpRequest('forgotpassword', 'post', {email: email}, (response) => {
		callback(response);
	});
}

export function callVerifyNumberApi(mobile, callback) {
	utils.httpRequest('sendotp', 'post', {mobile: mobile}, (response) => {
		callback(response);
	});
}

export function callVerifyOTPApi(obj, callback) {
	utils.httpRequest('verifyotp', 'post', obj, (response) => {
		callback(response);
	});
}

export function callUserDetailsApi(callback) {
	utils.httpRequest('users/myprofile', 'get', null, (response) => {
		callback(response);
	});
}
