import React from 'react';
import "../../../css/footer.css";

const Footer = (props) => {
    return <footer>
        <div className="container flexible wrap">
            <div className="footer-info">
                <img className="footer-logo" alt="Footer Logo" src="/images/logo.png" />
                <p>
                    <i className="fa fa-phone"></i>
                    <a href="tel:8448187401">8448187401</a>
                </p>
                <p>
                    <i className="fa fa-envelope"></i>
                    <a href="mailto:shweta@eduexcellence">shweta@eduexcellence</a>
                </p>

                <ul className="social-footer">
                    <li>
                        <a href="#">
                            <i className="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i className="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i className="fa fa-google-plus"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i className="fa fa-pinterest"></i>
                        </a>
                    </li>
                </ul>

            </div>
            <div className="imp-links">
                <h4>Company</h4>
                <ul>
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Testimonials</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                    <li>
                        <a href="#">All Teachers</a>
                    </li>
                </ul>
            </div>
            <div className="imp-links">
                <h4>Links</h4>
                <ul>
                    <li>
                        <a href="#">Courses</a>
                    </li>
                    <li>
                        <a href="#">Events</a>
                    </li>
                    <li>
                        <a href="#">Gallery</a>
                    </li>
                    <li>
                        <a href="#">FAQs</a>
                    </li>
                </ul>
            </div>

            <div className="imp-links">
                <h4>Support</h4>
                <ul>
                    <li>
                        <a href="#">Documentation</a>
                    </li>
                    <li>
                        <a href="#">Research</a>
                    </li>
                    <li>
                        <a href="#">Our Commitment</a>
                    </li>
                    <li>
                        <a href="#">Our Advisors</a>
                    </li>
                </ul>
            </div>

            <div className="imp-links">
                <h4>Subscription For Newsletter</h4>
                <input type="text" className="footer-input" placeholder="Enter Email" />
                <button className="footerbtn">Subscribe</button>
            </div>
        </div>
    </footer>
}

export default Footer;