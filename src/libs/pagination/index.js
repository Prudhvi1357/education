import React from "react";

export default (props) => {

    let no_of_pages = Math.ceil(props.count/props.items);

    const renderNumbers = () => {
        let items = [];
        let st = props.page - 2;
        st = st > 0 ? st : 1;
        for(let i = st, j = 0; i <= no_of_pages && j < 5; i++,j++ ) {
            items.push(<span className={i === props.page + 1 ? "active" : ""} key={i} onClick={() => props.callback(i - 1)}>{i}</span>);
        }
        return items;
    }

    return <div className="pagination-cus">
        <div className="row">
            <div className="col-lg-3">
                <p>Page : &nbsp; {props.page + 1} of {no_of_pages}</p>
            </div>
            {/* {props.page > 1 && <div className="col-lg-2 text-left">
                <button onClick={() => props.callback(props.page - 1)}>Prev</button>
            </div>} */}
            <div className="col-lg-7">
                <p>{renderNumbers()}</p>
            </div>
            {props.page + 1 < no_of_pages && <div className="col-lg-2 text-right">
                <button onClick={() => props.callback(props.page + 1)}>Next</button>
            </div>}
        </div>
    </div>
}


