$(document).ready(function () {

  $('#play-video').on('click', function (e) {
    e.preventDefault();
    $('#video-overlay').addClass('open');
    $("#video-overlay").append('<iframe width="560" height="315" src="https://www.youtube.com/embed/ngElkyQ6Rhs" frameborder="0" allowfullscreen></iframe>');
  });

  $('.video-overlay, .video-overlay-close').on('click', function (e) {
    e.preventDefault();
    close_video();
  });

  $(document).keyup(function (e) {
    if (e.keyCode === 27) { close_video(); }
  });

  function close_video() {
    $('.video-overlay.open').removeClass('open').find('iframe').remove();
  };



  $(".stu-trri").click(function () {
    $(".overlay-stu").toggle();
  });

  $(".teach-trri").click(function () {
    $(".overlay-teach").toggle();
  });



  window.onscroll = function () { myFunction() };

  var header = document.getElementById("myHeader");
  var sticky = header ? header.offsetTop : 0;

  function myFunction() {
    if (window.pageYOffset > sticky) {
      // header.classList.add("sticky");
    } else {
      // header.classList.remove("sticky");
    }
  }



  // jQuery("#banner-carousel").owlCarousel({
  //   autoplay: true,
  //   lazyLoad: true,
  //   loop: true,
  //   margin: 20,
  //   /*
  //  animateOut: 'fadeOut',
  //  animateIn: 'fadeIn',
  //  */
  //   responsiveClass: true,
  //   autoHeight: true,
  //   autoplayTimeout: 7000,
  //   smartSpeed: 800,
  //   nav: true,
  //   responsive: {
  //     0: {
  //       items: 1
  //     },

  //     600: {
  //       items: 1
  //     },

  //     1024: {
  //       items: 1
  //     },

  //     1366: {
  //       items: 1
  //     }
  //   }
  // });


  // $('.courses-slider').owlCarousel({
  //   loop: true,
  //   margin: 22,
  //   autoplayTimeout: 3000,
  //   smartSpeed: 200,
  //   nav: true,
  //   navText: [
  //     "<i class='fa fa-angle-left'></i>",
  //     "<i class='fa fa-angle-right'></i>"
  //   ],
  //   autoplay: true,
  //   autoplayHoverPause: true,
  //   responsive: {
  //     0: {
  //       items: 1
  //     },
  //     600: {
  //       items: 2
  //     },
  //     1000: {
  //       items: 3
  //     }
  //   }
  // });



  // $('#customers-testimonials').owlCarousel({
  //   loop: true,
  //   center: true,
  //   autoplay: true,
  //   autoplayTimeout: 3000,
  //   smartSpeed: 800,
  //   responsive: {
  //     0: { items: 1 },
  //     768: { items: 1 },
  //     1170: { items: 3 }
  //   }
  // });



  // jQuery("#strip-carousel").owlCarousel({
  //   autoplay: true,
  //   lazyLoad: true,
  //   loop: true,
  //   margin: 20,
  //   /*
  //  animateOut: 'fadeOut',
  //  animateIn: 'fadeIn',
  //  */
  //   responsiveClass: true,
  //   autoHeight: true,
  //   autoplayTimeout: 7000,
  //   smartSpeed: 800,
  //   nav: true,
  //   responsive: {
  //     0: {
  //       items: 2
  //     },

  //     600: {
  //       items: 3
  //     },

  //     1024: {
  //       items: 5
  //     },

  //     1366: {
  //       items: 5
  //     }
  //   }
  // });



  // jQuery("#blog-carousel").owlCarousel({
  //   autoplay: true,
  //   lazyLoad: true,
  //   loop: true,
  //   margin: 20,
  //   /*
  //  animateOut: 'fadeOut',
  //  animateIn: 'fadeIn',
  //  */
  //   responsiveClass: true,
  //   autoHeight: true,
  //   autoplayTimeout: 7000,
  //   smartSpeed: 800,
  //   nav: true,
  //   responsive: {
  //     0: {
  //       items: 1
  //     },

  //     600: {
  //       items: 2
  //     },

  //     1024: {
  //       items: 4
  //     },

  //     1366: {
  //       items: 4
  //     }
  //   }
  // });




  // $(".datepicker").datepicker({
  //   prevText: '<i class="fa fa-fw fa-angle-left"></i>',
  //   nextText: '<i class="fa fa-fw fa-angle-right"></i>'
  // });



});












jQuery(window).scroll(function () {
  if (jQuery(window).scrollTop() < 50) {
    jQuery('#rocketmeluncur').slideUp(500);
  } else {
    jQuery('#rocketmeluncur').slideDown(500);
  }
  let ftrocketmeluncur = jQuery("#ft")[0] ? jQuery("#ft")[0] : jQuery(document.body)[0];
  let scrolltoprocketmeluncur = $('#rocketmeluncur');
  if (scrolltoprocketmeluncur && scrolltoprocketmeluncur.style) {
    var viewPortHeightrocketmeluncur = parseInt(document.documentElement.clientHeight);
    var scrollHeightrocketmeluncur = parseInt(document.body.getBoundingClientRect().top);
    var basewrocketmeluncur = parseInt(ftrocketmeluncur.clientWidth);
    var swrocketmeluncur = scrolltoprocketmeluncur.clientWidth;
    if (basewrocketmeluncur < 1000) {
      var leftrocketmeluncur = parseInt(fetchOffset(ftrocketmeluncur)['left']);
      leftrocketmeluncur = leftrocketmeluncur < swrocketmeluncur ? leftrocketmeluncur * 2 - swrocketmeluncur : leftrocketmeluncur;
      scrolltoprocketmeluncur.style.left = (basewrocketmeluncur + leftrocketmeluncur) + 'px';
    } else {
      scrolltoprocketmeluncur.style.left = 'auto';
      scrolltoprocketmeluncur.style.right = '10px';
    }
  }
})

jQuery('#rocketmeluncur').click(function () {
  jQuery("html, body").animate({ scrollTop: '0px', display: 'none' }, {
    duration: 600,
    easing: 'linear'
  });

  var self = this;
  this.className += ' ' + "launchrocket";
  setTimeout(function () {
    self.className = 'showrocket';
  }, 800)
});






jQuery(function ($) {

  var $question = $('.question');
  var $answer = $('.answer');

  $question.click(function () {

    // Hide all answers
    $answer.slideUp();

    // Check if this answer is already open
    if ($(this).hasClass('open')) {
      // If already open, remove 'open' class and hide answer
      $(this).removeClass('open')
        .next($answer).slideUp();
      // If it is not open...
    } else {
      // Remove 'open' class from all other questions
      $question.removeClass('open');
      // Open this answer and add 'open' class
      $(this).addClass('open')
        .next($answer).slideDown();

    }
  });

});